﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Projekat.Models
{
    public class MjestoOdrzavanja
    {

        private string adresa;

        public string Adresa
        {
            get { return adresa; }
            set { adresa = value; }
        }

        public MjestoOdrzavanja()
        {
        }

        public MjestoOdrzavanja(string adresa)
        {
            Adresa = adresa;
        }
    }
}