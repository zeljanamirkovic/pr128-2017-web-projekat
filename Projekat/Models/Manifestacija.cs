﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Projekat.Models
{
    public class Manifestacija
    {

        private string naziv;

        public string Naziv
        {
            get { return naziv; }
            set { naziv = value; }
        }

        private ManifestacijaTip tipManifestacije;

        public ManifestacijaTip TipManifestacije
        {
            get { return tipManifestacije; }
            set { tipManifestacije = value; }
        }

        private int brojMjesta;

        public int BrojMjesta

        {
            get { return brojMjesta; }
            set { brojMjesta = value; }
        }

        private DateTime datumVrijemeOdrzavanja;

        public DateTime DatumVrijemeOdrzavanja
        {
            get { return datumVrijemeOdrzavanja; }
            set { datumVrijemeOdrzavanja = value; }
        }

        private double cijenaRegularKarte;

        public double CijenaRegularKarte
        {
            get { return cijenaRegularKarte; }
            set { cijenaRegularKarte = value; }
        }

        private StatusTip status;

        public StatusTip Status
        {
            get { return status; }
            set { status = value; }
        }

        private string mjestoOdrzavanja;

        public string MjestoOdrzavanja
        {
            get { return mjestoOdrzavanja; }
            set { mjestoOdrzavanja = value; }
        }

        private string posterManifestacije;

        public string PosterManifestacije
        {
            get { return posterManifestacije; }
            set { posterManifestacije = value; }
        }

        private string lokacija;

        public string Lokacija
        {
            get { return lokacija; }
            set { lokacija = value; }
        }

        private string korisnickoIme;

        public string KorisnickoIme
        {
            get { return korisnickoIme; }
            set { korisnickoIme = value; }
        }

        private double ocjena;

        public double Ocjena
        {
            get { return ocjena; }
            set { ocjena = value; }
        }

        private bool obrisana;

        public bool Obrisana
        {
            get { return obrisana; }
            set { obrisana = value; }
        }

        private int brojRezervisanihKarata;

        public int BrojRezervisanihKarata
        {
            get { return brojRezervisanihKarata; }
            set { brojRezervisanihKarata = value; }
        }

        public Manifestacija()
        {
           // DatumVrijemeOdrzavanja = DateTime.Now;
        }

        public Manifestacija(string naziv, ManifestacijaTip tipManifestacije, int brojMjesta, DateTime datumVrijemeOdrzavanja, double cijenaRegularKarte, StatusTip status, string mjestoOdrzavanja,/* string posterManifestacije,*/ string lokacija, string korisnickoIme, double ocjena, bool obrisana, int brojRezerviranihKarata)
        {
            Naziv = naziv;
            TipManifestacije = tipManifestacije;
            BrojMjesta = brojMjesta;
            DatumVrijemeOdrzavanja = datumVrijemeOdrzavanja; 
            CijenaRegularKarte = cijenaRegularKarte;
            Status = status;
            MjestoOdrzavanja = mjestoOdrzavanja;
           // PosterManifestacije = posterManifestacije;
            Lokacija = lokacija;
            KorisnickoIme = korisnickoIme;
            Ocjena = ocjena;
            Obrisana = obrisana;
            BrojRezervisanihKarata = brojRezerviranihKarata;
        }
    }
}