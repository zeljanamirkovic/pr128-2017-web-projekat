﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;

namespace Projekat.Models
{
    public class Data
    {

        public static void SacuvajKorisnika(Korisnik korisnik)
        {
            string putanja = HostingEnvironment.MapPath("~/App_Data/korisnici.txt");
            FileStream stream = new FileStream(putanja, FileMode.Append);
            StreamWriter sw = new StreamWriter(stream);

            //  if (korisnik.Uloga != Models.UlogaTip.Administrator)
            // {
            sw.WriteLine(korisnik.KorisnickoIme + ";" + korisnik.Lozinka + ";" + korisnik.Ime + ";" +
                korisnik.Prezime + ";" + korisnik.Pol + ";" + korisnik.DatumRodjenja + ";" + "Kupac" + ";" + korisnik.SveKarteBezObziraNaStatus + ";"
                + korisnik.Manifestacije + ";" + korisnik.BrojSakupljenihBodova + ";" + korisnik.TipKorisnika.ImeTipa + ";" + "false");
            //}

            sw.Close();
            stream.Close();
        }

        public static List<Korisnik> ReadUsers(string path)
        {
            List<Korisnik> korisnici = new List<Korisnik>();
            List<Karta> karta = new List<Karta>();
            List<Manifestacija> manifestacija = new List<Manifestacija>();
            TKImeTip tip = new TKImeTip();

            path = HostingEnvironment.MapPath(path);
            FileStream stream = new FileStream(path, FileMode.Open);
            StreamReader sr = new StreamReader(stream);
            string line = "";
            while ((line = sr.ReadLine()) != null)
            {
                string[] podaci = line.Split(';');

                if (podaci[10] == "Zlatni")
                {
                    tip = TKImeTip.Zlatni;
                }
                else if (podaci[10] == "Bronzani")
                {
                    tip = TKImeTip.Bronzani;
                }else if (podaci[10] == "Srebrni")
                {
                    tip = TKImeTip.Srebrni;
                }

                Korisnik k = new Korisnik(podaci[0], podaci[1], podaci[2], podaci[3],
                    (PolTip)Enum.Parse(typeof(PolTip), podaci[4]), DateTime.Parse(podaci[5]),
                    (UlogaTip)Enum.Parse(typeof(UlogaTip), podaci[6]), karta, manifestacija,
                    Int32.Parse(podaci[9]), new TipKorisnika(tip, 0, 0), Boolean.Parse(podaci[11]));
                korisnici.Add(k);
            }
            sr.Close();
            return korisnici;
        }

        public static void SacuvajManifestaciju(Manifestacija manifestacija)
        {
            string putanja = HostingEnvironment.MapPath("~/App_Data/manifestacije.txt");
            FileStream stream = new FileStream(putanja, FileMode.Append);
            StreamWriter sw = new StreamWriter(stream);

            // if (korisnik.Uloga != Models.UlogaTip.Administrator)
            //  {
            sw.WriteLine(manifestacija.Naziv + ";" + manifestacija.TipManifestacije + ";" + manifestacija.BrojMjesta + ";"
                + manifestacija.DatumVrijemeOdrzavanja + ";" + manifestacija.CijenaRegularKarte + ";" + "Neaktivno" + ";" + manifestacija.MjestoOdrzavanja + ";"
                + manifestacija.Lokacija + ";" + manifestacija.KorisnickoIme + ";" + manifestacija.Ocjena + ";" + "false" + ";" + manifestacija.BrojRezervisanihKarata);      //}

            sw.Close();
            stream.Close();
        }

        public static List<Manifestacija> ProcitajManifestacije(string path)
        {
            List<Manifestacija> manifestacije = new List<Manifestacija>();
           
            path = HostingEnvironment.MapPath(path);
            FileStream stream = new FileStream(path, FileMode.Open);
            StreamReader sr = new StreamReader(stream);
            string line = "";

            while ((line = sr.ReadLine()) != null)
            {
               
                string[] podaci = line.Split(';');
                Manifestacija m = new Manifestacija(podaci[0], (ManifestacijaTip)Enum.Parse(typeof(ManifestacijaTip), podaci[1]), Int32.Parse(podaci[2]), 
                    DateTime.Parse(podaci[3]), Double.Parse(podaci[4]), (StatusTip)Enum.Parse(typeof(StatusTip), podaci[5]), podaci[6], podaci[7], podaci[8], 
                    Double.Parse(podaci[9]), Boolean.Parse(podaci[10]), Int32.Parse(podaci[11]));
                manifestacije.Add(m);
            }
            sr.Close();
            return manifestacije;
        }
       

        public static void SacuvajProdavca(Korisnik korisnik)
        {
            string putanja = HostingEnvironment.MapPath("~/App_Data/korisnici.txt");
            FileStream stream = new FileStream(putanja, FileMode.Append);
            StreamWriter sw = new StreamWriter(stream);

            // if (korisnik.Uloga != Models.UlogaTip.Administrator)
            //  {
            sw.WriteLine(korisnik.KorisnickoIme + ";" + korisnik.Lozinka + ";" + korisnik.Ime + ";" +
                 korisnik.Prezime + ";" + korisnik.Pol + ";" + korisnik.DatumRodjenja + ";" + "Prodavac" + ";" + korisnik.SveKarteBezObziraNaStatus + ";" + korisnik.Manifestacije + ";" + korisnik.BrojSakupljenihBodova + ";" + "Bronzani" + ";" + "false");
            //}

        sw.Close();
            stream.Close();
        }

   
        public static void SacuvajKarte(List<Karta> karte)
        {
            string putanja = HostingEnvironment.MapPath("~/App_Data/karte.txt");
            FileStream stream = new FileStream(putanja, FileMode.Append);
            StreamWriter sw = new StreamWriter(stream);

            foreach(Karta k in karte) { 
            sw.WriteLine(k.JedinstveniIdentifikatorKarte + ";" + k.Manifestacija.Lokacija + ";" +
                k.Manifestacija.DatumVrijemeOdrzavanja + ";"+ k.DatumVrijemeManifestacije+ ";" + k.Cijena + ";"+ k.Kupac+";" + k.Status + ";"  +k.Tip
              );      //}
            }
            sw.Close();
            stream.Close();
        }

        public static List<Karta> ProcitajKarte(string path)
        {
            List<Karta> karte = new List<Karta>();
            List<Manifestacija> manifestacije = Data.ProcitajManifestacije("~/App_Data/manifestacije.txt");
            Manifestacija manifestacija = new Manifestacija();
            path = HostingEnvironment.MapPath(path);
            FileStream stream = new FileStream(path, FileMode.Open);
            StreamReader sr = new StreamReader(stream);
            string line = "";
            while ((line = sr.ReadLine()) != null)
            {
                string[] podaci = line.Split(';');
                foreach(Manifestacija item in manifestacije)
                {
                    if(item.Lokacija.ToUpper().Equals(podaci[1].ToUpper()) && item.DatumVrijemeOdrzavanja.Equals(DateTime.Parse(podaci[2])))
                    {
                        manifestacija = item;
                        break;
                    }
                }
              //  manifestacija = manifestacije.Find(m => m.Lokacija.Equals(podaci[1]) && m.DatumVrijemeOdrzavanja.Equals(DateTime.Parse(podaci[2])));
                Karta k = new Karta(podaci[0], manifestacija, DateTime.Parse(podaci[3]), Double.Parse(podaci[4]), podaci[5],
                    (StatusKarta)Enum.Parse(typeof(StatusKarta), podaci[6]), (TipKarta)Enum.Parse(typeof(TipKarta), podaci[7]));
                karte.Add(k);
            }
            sr.Close();
            return karte;
        }


        public static void SacuvajKomentar(Komentar komentar)
        {
            string putanja = HostingEnvironment.MapPath("~/App_Data/komentari.txt");
            FileStream stream = new FileStream(putanja, FileMode.Append);
            StreamWriter sw = new StreamWriter(stream);

            //  if (korisnik.Uloga != Models.UlogaTip.Administrator)
            // {
            sw.WriteLine(komentar.KupacKarte + ";" + komentar.ManifestacijaKomentar.Lokacija + ";" + komentar.ManifestacijaKomentar.DatumVrijemeOdrzavanja
                + ";" + komentar.TekstKomentara + ";" + komentar.Ocjena + ";" + komentar.Status + ";" + komentar.IdKomentara);

            sw.Close();
            stream.Close();
        }

        public static List<Komentar> ProcitajKomentare(string path)
        {
            List<Komentar> komentari = new List<Komentar>();
            List<Manifestacija> manifestacije = Data.ProcitajManifestacije("~/App_Data/manifestacije.txt");

            Manifestacija manifestacija = new Manifestacija();

            path = HostingEnvironment.MapPath(path);
            FileStream stream = new FileStream(path, FileMode.Open);
            StreamReader sr = new StreamReader(stream);
            string line = "";
            while ((line = sr.ReadLine()) != null)
            {
                string[] podaci = line.Split(';');
                foreach (Manifestacija item in manifestacije)
                {
                    if (item.Lokacija.Equals(podaci[1]) && item.DatumVrijemeOdrzavanja.Equals(DateTime.Parse(podaci[2])))
                    {
                        manifestacija = item;
                        break;
                    }
                }
                Komentar k = new Komentar(podaci[0], manifestacija, podaci[3], Int32.Parse(podaci[4]), (StatusTip)Enum.Parse(typeof(StatusTip), podaci[5]), Int32.Parse(podaci[6]));
                komentari.Add(k);
            }
            sr.Close();
            return komentari;
        }

    }
}