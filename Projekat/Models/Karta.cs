﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Projekat.Models
{
    public class Karta
    {
        private string jedinstveniIdentifikatorKarte;

        public string JedinstveniIdentifikatorKarte
        {
            get { return jedinstveniIdentifikatorKarte; }
            set { jedinstveniIdentifikatorKarte = value; }
        }


        private Manifestacija manifestacija;

        public Manifestacija Manifestacija
        {
            get { return manifestacija; }
            set { manifestacija = value; }
        }

        private DateTime datumVrijemeManifestacije;

        public DateTime DatumVrijemeManifestacije
        {
            get { return datumVrijemeManifestacije; }
            set { datumVrijemeManifestacije = value; }
        }

        private double cijena;

        public double Cijena
        {
            get { return cijena; }
            set { cijena = value; }
        }

        private string kupac;

        public string Kupac
        {
            get { return kupac; }
            set { kupac = value; }
        }

        private StatusKarta status;

        public StatusKarta Status
        {
            get { return status; }
            set { status = value; }
        }
        private TipKarta tip;

        public TipKarta Tip
        {
            get { return tip; }
            set { tip = value; }
        }

       /* private DateTime myVar;

        public DateTime MyProperty
        {
            get { return myVar; }
            set { myVar = value; }
        }*/

        public Karta()
        {
        }

        public Karta(string jedinstveniIdentifikatorKarte, Manifestacija manifestacija, DateTime datumVrijemeManifestacije, double cijena, string kupac, StatusKarta status, TipKarta tip)
        {
            JedinstveniIdentifikatorKarte = jedinstveniIdentifikatorKarte;
            Manifestacija = manifestacija;
            DatumVrijemeManifestacije = datumVrijemeManifestacije;
            Cijena = cijena;
            Kupac = kupac;
            Status = status;
            Tip = tip;
        }
    }
}