﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Projekat.Models
{
    public enum UlogaTip
    {
        Administrator,
        Prodavac,
        Kupac
    }
}