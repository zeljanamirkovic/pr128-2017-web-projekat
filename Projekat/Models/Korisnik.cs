﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Projekat.Models
{
    public class Korisnik
    {
        private string korisnickoIme;

        public string KorisnickoIme
        {
            get { return korisnickoIme; }
            set { korisnickoIme = value; }
        }

        private string lozinka;

        public string Lozinka
        {
            get { return lozinka; }
            set { lozinka = value; }
        }

        private string ime;

        public string Ime
        {
            get { return ime; }
            set { ime = value; }
        }

        private string prezime;

        public string Prezime
        {
            get { return prezime; }
            set { prezime = value; }
        }

        private PolTip pol;

        public PolTip Pol
        {
            get { return pol; }
            set { pol = value; }
        }

        private DateTime datumRodjenja;

        public DateTime DatumRodjenja
        {
            get { return datumRodjenja; }
            set { datumRodjenja = value; }
        }

        private UlogaTip uloga;

        public UlogaTip Uloga
        {
            get { return uloga; }
            set { uloga = value; }
        }

        private List<Karta> sveKarteBezObziraNaStatus;

        public List<Karta> SveKarteBezObziraNaStatus
        {
            get { return sveKarteBezObziraNaStatus; }
            set { sveKarteBezObziraNaStatus = value; }
        }


        private List<Manifestacija> manifestacije;

        public List<Manifestacija> Manifestacije
        {
            get { return manifestacije; }
            set { manifestacije = value; }
        }

        private int brojSakupljenihBodova;

        public int BrojSakupljenihBodova
        {
            get { return brojSakupljenihBodova; }
            set { brojSakupljenihBodova = value; }
        }

        private TipKorisnika tipKorisnika;

        public TipKorisnika TipKorisnika
        {
            get { return tipKorisnika; }
            set { tipKorisnika = value; }
        }

        private bool obrisan;

        public bool Obrisan
        {
            get { return obrisan; }
            set { obrisan = value; }
        }

        public Korisnik()
        {
            KorisnickoIme = "";
            Lozinka = "";
            BrojSakupljenihBodova = 0;
            TipKorisnika = new TipKorisnika(TKImeTip.Bronzani,0,0);

        }

        public Korisnik(string korisnickoIme, string lozinka, string ime, string prezime, PolTip pol, DateTime datumRodjenja, UlogaTip uloga, List<Karta> sveKarteBezObziraNaStatus, List<Manifestacija> manifestacije, int brojSakupljenihBodova, TipKorisnika tipKorisnika, bool obrisan)
        {
            KorisnickoIme = korisnickoIme;
            Lozinka = lozinka;
            Ime = ime;
            Prezime = prezime;
            Pol = pol;
            DatumRodjenja = datumRodjenja;
            Uloga = uloga;
            SveKarteBezObziraNaStatus = sveKarteBezObziraNaStatus;
            Manifestacije = manifestacije;
            BrojSakupljenihBodova = brojSakupljenihBodova;
            TipKorisnika = tipKorisnika;
            Obrisan = obrisan;
        }
    }
}