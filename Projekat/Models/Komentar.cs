﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Projekat.Models
{
    public class Komentar
    {
        private string kupacKarte;

        public string KupacKarte
        {
            get { return kupacKarte; }
            set { kupacKarte = value; }
        }

        private Manifestacija manifestacijaKomentar;

        public Manifestacija ManifestacijaKomentar
        {
            get { return manifestacijaKomentar; }
            set { manifestacijaKomentar = value; }
        }


        private string tesktKomentara;

        public string TekstKomentara
        {
            get { return tesktKomentara; }
            set { tesktKomentara = value; }
        }

        private int ocjena;

        public int Ocjena
        {
            get { return ocjena; }
            set { ocjena = value; }
        }

        private StatusTip status;

        public StatusTip Status
        {
            get { return status; }
            set { status = value; }
        }

        private int idKomentara;

        public int IdKomentara
        {
            get { return idKomentara; }
            set { idKomentara = value; }
        }

        public Komentar()
        {
        }

        public Komentar(string kupacKarte, Manifestacija manifestacijaKomentar, string tekstKomentara, int ocjena, StatusTip status, int idKomentara)
        {
            KupacKarte = kupacKarte;
            ManifestacijaKomentar = manifestacijaKomentar;
            TekstKomentara = tekstKomentara;
            Ocjena = ocjena;
            Status = status;
            IdKomentara = idKomentara;
        }
    }
}