﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Projekat.Models
{
    public class Lokacija
    {
        private string geografskaDuzina;

        public string GeografskaDuzina
        {
            get { return geografskaDuzina; }
            set { geografskaDuzina = value; }
        }

        private string geografskaSirina;

        public string GeografskaSirina
        {
            get { return geografskaSirina; }
            set { geografskaSirina = value; }
        }

        private MjestoOdrzavanja mjestoOdrzavanja;

        public MjestoOdrzavanja MjestoOdrzavanja
        {
            get { return mjestoOdrzavanja; }
            set { mjestoOdrzavanja = value; }
        }

        public Lokacija()
        {
        }

        public Lokacija(string geografskaDuzina, string geografskaSirina, MjestoOdrzavanja mjestoOdrzavanja)
        {
            GeografskaDuzina = geografskaDuzina;
            GeografskaSirina = geografskaSirina;
            MjestoOdrzavanja = mjestoOdrzavanja;
        }
    }
}