﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Projekat.Models
{
    public class TipKorisnika
    {
        private TKImeTip imeTipa;

        public TKImeTip ImeTipa
        {
            get { return imeTipa; }
            set { imeTipa = value; }
        }

        private double popust;

        public double Popust
        {
            get { return popust; }
            set { popust = value; }
        }

        private int trazeniBrojBodova;

        public int TrazeniBrojBodova
        {
            get { return trazeniBrojBodova; }
            set { trazeniBrojBodova = value; }
        }

        public TipKorisnika()
        {
            
        }

        public TipKorisnika(TKImeTip imeTipa, double popust, int trazeniBrojBodova)
        {
            ImeTipa = imeTipa;
            Popust = popust;
            TrazeniBrojBodova = trazeniBrojBodova;
        }
    }
}