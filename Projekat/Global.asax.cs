﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Projekat.Models;

namespace Projekat
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            List<Korisnik> korisnici = Data.ReadUsers("~/App_Data/korisnici.txt");
            HttpContext.Current.Application["korisnici"] = korisnici;

            List<Manifestacija> manifestacije = Data.ProcitajManifestacije("~/App_Data/manifestacije.txt");
            HttpContext.Current.Application["manifestacije"] = manifestacije;

            List<Karta> karte = Data.ProcitajKarte("~/App_Data/karte.txt");
            HttpContext.Current.Application["karte"] = karte;
        }
    }
}
