﻿using Projekat.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;

namespace Projekat.Controllers
{
    public class ProdavacController : Controller
    {
        // GET: Prodavac
        public ActionResult ListaManifestacija()
        {
            Korisnik k = (Korisnik)Session["korisnik"];
            if (k == null)
                return RedirectToAction("Prijava", "Autentifikacija");


            List<Manifestacija> rezultat = new List<Manifestacija>();
            List<Manifestacija> listaManifestacija = Data.ProcitajManifestacije("~/App_Data/manifestacije.txt");

            for (int i = 0; i < listaManifestacija.Count; i++)
            {
                if (listaManifestacija[i].Obrisana == false && listaManifestacija[i].KorisnickoIme == k.KorisnickoIme)
                {
                    rezultat.Add(listaManifestacija[i]);
                }
            }

            ViewBag.manifestacija = rezultat;

            return View("~/Views/Prodavac/ListaManifestacija.cshtml", rezultat);
        }

        public ActionResult IzmijeniManifestaciju(string lokacija, string datumVrijemeOdrzavanja)
        {
            Manifestacija manifestacija = new Manifestacija();
            List<Manifestacija> listaManifestacija = Data.ProcitajManifestacije("~/App_Data/manifestacije.txt");

            for (int i = 0; i < listaManifestacija.Count; i++)
            {
                if (listaManifestacija[i].Obrisana == false && listaManifestacija[i].Lokacija.ToUpper() == lokacija.ToUpper() && listaManifestacija[i].DatumVrijemeOdrzavanja.ToString() == datumVrijemeOdrzavanja)
                {
                    manifestacija = listaManifestacija[i];
                    break;
                }
            }

            ViewBag.manifestacija = manifestacija;
            ViewBag.stara = manifestacija;
            //ViewBag.Message = "";
            return View("~/Views/Prodavac/IzmijeniManifestaciju.cshtml", manifestacija);
        }

        [HttpPost]
        public ActionResult IzmijeniManifestaciju(Manifestacija nova, string lokacija, string datumVrijeme)
        {
            List<Karta> karte = Data.ProcitajKarte("~/App_Data/karte.txt");
            List<Komentar> komentari = Data.ProcitajKomentare("~/App_Data/komentari.txt");
            List<Manifestacija> listaManifestacija = Data.ProcitajManifestacije("~/App_Data/manifestacije.txt");

            //-----------------------------------prvo promijenin karte, jer kasnije mi puca zato sto poredim sa manifestacijom kod koje sam vec promijenila datum i lokaciju--------------------------------
            try
            {
                karte.Find(kar => kar.Manifestacija.Obrisana == false && kar.Manifestacija.Lokacija.ToUpper().Equals(lokacija.ToUpper()) && kar.Manifestacija.DatumVrijemeOdrzavanja.ToString().Equals(datumVrijeme)).Manifestacija.Lokacija = nova.Lokacija;
            }
            catch { }

            try
            {
                karte.Find(kar => kar.Manifestacija.Obrisana == false && kar.Manifestacija.Lokacija.ToUpper().Equals(lokacija.ToUpper()) && kar.Manifestacija.DatumVrijemeOdrzavanja.ToString().Equals(datumVrijeme)).Manifestacija.DatumVrijemeOdrzavanja = nova.DatumVrijemeOdrzavanja;
            }
            catch { }

            string path1 = HostingEnvironment.MapPath("~/App_Data/karte.txt");
            FileStream stream1 = new FileStream(path1, FileMode.Create);
            StreamWriter sw1 = new StreamWriter(stream1);

            foreach (Karta k in karte)
            {
                sw1.WriteLine(k.JedinstveniIdentifikatorKarte + ";" + k.Manifestacija.Lokacija + ";" +
               k.Manifestacija.DatumVrijemeOdrzavanja + ";" + k.DatumVrijemeManifestacije + ";" + k.Cijena + ";" + k.Kupac + ";" + k.Status + ";" + k.Tip);
            }

            sw1.Close();
            stream1.Close();

            //--------------------------------------------------------------------------------------------------------------           
            try
            {
                komentari.Find(kom => kom.ManifestacijaKomentar.Obrisana == false && kom.ManifestacijaKomentar.Lokacija.ToUpper().Equals(lokacija.ToUpper()) && kom.ManifestacijaKomentar.DatumVrijemeOdrzavanja.ToString().Equals(datumVrijeme)).ManifestacijaKomentar.Lokacija = nova.Lokacija;
            }
            catch { }

            try
            {
                komentari.Find(kom => kom.ManifestacijaKomentar.Obrisana == false && kom.ManifestacijaKomentar.Lokacija.ToUpper().Equals(lokacija.ToUpper()) && kom.ManifestacijaKomentar.DatumVrijemeOdrzavanja.ToString().Equals(datumVrijeme)).ManifestacijaKomentar.DatumVrijemeOdrzavanja = nova.DatumVrijemeOdrzavanja;
            }
            catch { }


            string path2 = HostingEnvironment.MapPath("~/App_Data/komentari.txt");
            FileStream stream2 = new FileStream(path2, FileMode.Create);
            StreamWriter sw2 = new StreamWriter(stream2);

            foreach (Komentar kom in komentari)
            {
                sw2.WriteLine(kom.KupacKarte + ";" + kom.ManifestacijaKomentar.Lokacija + ";" + kom.ManifestacijaKomentar.DatumVrijemeOdrzavanja
                + ";" + kom.TekstKomentara + ";" + kom.Ocjena + ";" + kom.Status + ";" + kom.IdKomentara);

            }
            //
            sw2.Close();
            stream2.Close();

            //--------------------------------------------------------------------------------------------------------------------------------------

            for (int i = 0; i < listaManifestacija.Count; i++)
            {
                if (listaManifestacija[i].Lokacija.ToUpper().Equals(lokacija.ToUpper()) && listaManifestacija[i].DatumVrijemeOdrzavanja.ToString().Equals(datumVrijeme) && listaManifestacija[i].Obrisana == false)
                {
                    listaManifestacija[i].Naziv = nova.Naziv;
                    listaManifestacija[i].MjestoOdrzavanja = nova.MjestoOdrzavanja;
                    listaManifestacija[i].DatumVrijemeOdrzavanja = nova.DatumVrijemeOdrzavanja;
                    listaManifestacija[i].Lokacija = nova.Lokacija;
                    listaManifestacija[i].BrojMjesta = nova.BrojMjesta;
                    listaManifestacija[i].CijenaRegularKarte = nova.CijenaRegularKarte;
                    listaManifestacija[i].TipManifestacije = nova.TipManifestacije;
                    break;
                }
            }
            string path = HostingEnvironment.MapPath("~/App_Data/manifestacije.txt");
            FileStream stream = new FileStream(path, FileMode.Create);
            StreamWriter sw = new StreamWriter(stream);

            foreach (Manifestacija item in listaManifestacija)
            {
                sw.WriteLine(item.Naziv + ";" + item.TipManifestacije + ";" + item.BrojMjesta + ";"
                + item.DatumVrijemeOdrzavanja + ";" + item.CijenaRegularKarte + ";" + item.Status + ";" + item.MjestoOdrzavanja + ";" + item.Lokacija + ";"
                + item.KorisnickoIme + ";" + item.Ocjena + ";" + item.Obrisana + ";" + item.BrojRezervisanihKarata);
            }
            //
            sw.Close();
            stream.Close();


            return RedirectToAction("ListaManifestacija", "Prodavac");
        }


        //---------------------------------------------------------------------------------------------------------------------------------------------

        public ActionResult DodajManifestaciju()
        {
            return View("~/Views/Prodavac/DodajManifestaciju.cshtml");
        }
        [HttpPost]
        public ActionResult DodajManifestaciju(Manifestacija manifestacija)
        {
            Korisnik korisnik = (Korisnik)Session["korisnik"];
            manifestacija.KorisnickoIme = korisnik.KorisnickoIme;
            List<Manifestacija> listaManifestacija = Data.ProcitajManifestacije("~/App_Data/manifestacije.txt");


            if (manifestacija.DatumVrijemeOdrzavanja > DateTime.Now)
            {
                for (int i = 0; i < listaManifestacija.Count; i++)
                {
                    if (listaManifestacija[i].Obrisana == false && listaManifestacija[i].Lokacija.ToUpper().Equals(manifestacija.Lokacija.ToUpper()) && listaManifestacija[i].DatumVrijemeOdrzavanja.Equals(manifestacija.DatumVrijemeOdrzavanja))
                    {
                        ViewBag.Message = $"Zakazana je druga manifestacija u {listaManifestacija[i].MjestoOdrzavanja}, {listaManifestacija[i].DatumVrijemeOdrzavanja}";
                        return View();
                    }
                }


                listaManifestacija.Add(manifestacija);
                Data.SacuvajManifestaciju(manifestacija);
                ViewBag.manifestacije = listaManifestacija;
                return RedirectToAction("ListaManifestacija", "Prodavac");
            }

            ViewBag.manifestacije = listaManifestacija;
            ViewBag.Message = $"Datum manifestacije mora biti veci od '{DateTime.Now}'";
            return View();
        }

        public ActionResult ListaRezervisanihKarata(string lokacija, string datumVrijemeOdrzavanja)
        {
            Korisnik korisnik = (Korisnik)Session["korisnik"];
            List<Karta> listaKarata = Data.ProcitajKarte("~/App_Data/karte.txt");
            List<Karta> rezervisaneKarte = new List<Karta>();


            for (int i = 0; i < listaKarata.Count; i++)
            {
                if (listaKarata[i].Status == StatusKarta.Rezervisana)
                {
                    if(listaKarata[i].Manifestacija.Obrisana==false && listaKarata[i].Manifestacija.Lokacija.ToUpper()==lokacija.ToUpper()
                        && listaKarata[i].Manifestacija.DatumVrijemeOdrzavanja.ToString() == datumVrijemeOdrzavanja)
                    {
                        rezervisaneKarte.Add(listaKarata[i]);
                    }
                }
            }

            ViewBag.rezervisaneKarte = rezervisaneKarte;
            return View("~/Views/Prodavac/ListaRezervisanihKarata.cshtml", rezervisaneKarte);
        }


        public ActionResult ListaKomentara(string lokacija, string datumVrijemeOdrzavanja)
        {
            List<Komentar> listaKomentara = Data.ProcitajKomentare("~/App_Data/komentari.txt");
            List<Manifestacija> listaManifestacija = Data.ProcitajManifestacije("~/App_Data/manifestacije.txt");
            List<Komentar> rezultat = new List<Komentar>();

            Manifestacija manifestacija = new Manifestacija();
            for (int i = 0; i < listaManifestacija.Count; i++)
            {
                if (listaManifestacija[i].Obrisana == false && listaManifestacija[i].Lokacija.ToUpper() == lokacija.ToUpper() && listaManifestacija[i].DatumVrijemeOdrzavanja.ToString() == datumVrijemeOdrzavanja)
                {
                    manifestacija = listaManifestacija[i];
                    break;
                }
            }
            for(int i=0; i<listaKomentara.Count; i++)
            {
                if(listaKomentara[i].ManifestacijaKomentar.Obrisana==false && listaKomentara[i].ManifestacijaKomentar.Lokacija==manifestacija.Lokacija
                    && listaKomentara[i].ManifestacijaKomentar.DatumVrijemeOdrzavanja == manifestacija.DatumVrijemeOdrzavanja)
                {
                    rezultat.Add(listaKomentara[i]);
                }
            }

            ViewBag.komentari = rezultat;
           
            return View("~/Views/Prodavac/Komentari.cshtml", rezultat);
        }

        public ActionResult PromjeniStatus(int id)
        {
            List<Manifestacija> listaManifestacija = Data.ProcitajManifestacije("~/App_Data/manifestacije.txt");
            List<Komentar> listaKomentara = Data.ProcitajKomentare("~/App_Data/komentari.txt");

            for(int i=0; i<listaKomentara.Count; i++)
            {
                if (listaKomentara[i].IdKomentara == id)
                {
                    listaKomentara[i].Status = StatusTip.Aktivno;
                    break;
                }
            }

            foreach (Manifestacija mItem in listaManifestacija)
            {
                int brojac = 0;
                double prosjecnaOcjena = 0;

                if (mItem.DatumVrijemeOdrzavanja < DateTime.Now && mItem.Obrisana==false)
                {
                   foreach (Komentar kom in listaKomentara)
                   {
                      if (kom.Status == StatusTip.Aktivno && mItem.Lokacija == kom.ManifestacijaKomentar.Lokacija && mItem.DatumVrijemeOdrzavanja == kom.ManifestacijaKomentar.DatumVrijemeOdrzavanja)
                      {
                         brojac++;
                         prosjecnaOcjena += kom.Ocjena;
                      }
                   }
                   if (brojac == 0)
                   {
                     prosjecnaOcjena = 0;
                   }
                   else
                   {
                     prosjecnaOcjena = prosjecnaOcjena / brojac;
                   }

                   for(int i=0; i<listaManifestacija.Count; i++)
                    {
                        if(listaManifestacija[i].Obrisana==false && listaManifestacija[i].Lokacija==mItem.Lokacija && listaManifestacija[i].DatumVrijemeOdrzavanja == mItem.DatumVrijemeOdrzavanja)
                        {
                            listaManifestacija[i].Ocjena = prosjecnaOcjena;
                            break;
                        }
                    }                  
                }
            }
            string path = HostingEnvironment.MapPath("~/App_Data/manifestacije.txt");
            FileStream stream = new FileStream(path, FileMode.Create);
            StreamWriter sw = new StreamWriter(stream);

            foreach (Manifestacija item in listaManifestacija)
            {
                sw.WriteLine(item.Naziv + ";" + item.TipManifestacije + ";" + item.BrojMjesta + ";"
                + item.DatumVrijemeOdrzavanja + ";" + item.CijenaRegularKarte + ";" + item.Status + ";" + item.MjestoOdrzavanja + ";"
                + item.Lokacija + ";" + item.KorisnickoIme + ";" + item.Ocjena +";"+ item.Obrisana + ";" + item.BrojRezervisanihKarata);     

            }
            sw.Close();
            stream.Close();


            string path2 = HostingEnvironment.MapPath("~/App_Data/komentari.txt");
            FileStream stream2 = new FileStream(path2, FileMode.Create);
            StreamWriter sw2 = new StreamWriter(stream2);

            foreach (Komentar item in listaKomentara)
            {
                sw2.WriteLine(item.KupacKarte + ";" + item.ManifestacijaKomentar.Lokacija + ";" + item.ManifestacijaKomentar.DatumVrijemeOdrzavanja
                + ";" + item.TekstKomentara + ";" + item.Ocjena + ";" + item.Status + ";" + item.IdKomentara);

            }
            //
            sw2.Close();
            stream2.Close();
           
            return RedirectToAction("ListaManifestacija", "Prodavac");

        }

        public ActionResult Profil()
        {
            List<Korisnik> listaKorisnika = Data.ReadUsers("~/App_Data/korisnici.txt");
            Korisnik k = (Korisnik)Session["korisnik"];

            Korisnik izmijenjen = new Korisnik();
            foreach (Korisnik item in listaKorisnika)
            {
                if (item.KorisnickoIme == k.KorisnickoIme)
                {
                    if (!item.Obrisan)
                    {
                        izmijenjen = item;
                        break;
                    }
                }

            }
            return View("~/Views/Prodavac/Profil.cshtml", izmijenjen);

        }

        [HttpPost]
        public ActionResult Profil(Korisnik korisnik)
        {
            Korisnik k = (Korisnik)Session["korisnik"];
            List<Korisnik> listaKorisnika = Data.ReadUsers("~/App_Data/korisnici.txt");

            if (korisnik.KorisnickoIme == null || korisnik.Lozinka == null || korisnik.Ime == null || korisnik.Prezime == null)
            {
                //ViewBag.Message = $"Ne sme biti prazno polje";
                return RedirectToAction("Profil", "Prodavac");
            }

            listaKorisnika.Find(kor => kor.KorisnickoIme.Equals(k.KorisnickoIme) && kor.Obrisan == false).Lozinka = korisnik.Lozinka;
            listaKorisnika.Find(kor => kor.KorisnickoIme.Equals(k.KorisnickoIme) && kor.Obrisan == false).Ime = korisnik.Ime;
            listaKorisnika.Find(kor => kor.KorisnickoIme.Equals(k.KorisnickoIme) && kor.Obrisan == false).Prezime = korisnik.Prezime;
            listaKorisnika.Find(kor => kor.KorisnickoIme.Equals(k.KorisnickoIme) && kor.Obrisan == false).Pol = korisnik.Pol;
            listaKorisnika.Find(kor => kor.KorisnickoIme.Equals(k.KorisnickoIme) && kor.Obrisan == false).DatumRodjenja = korisnik.DatumRodjenja;

            string putanja = HostingEnvironment.MapPath("~/App_Data/korisnici.txt");
            FileStream stream = new FileStream(putanja, FileMode.Create);
            StreamWriter writer = new StreamWriter(stream);
            foreach (Korisnik item in listaKorisnika)
            {

                writer.WriteLine(item.KorisnickoIme + ";" + item.Lozinka + ";" + item.Ime + ";" +
               item.Prezime + ";" + item.Pol + ";" + item.DatumRodjenja + ";" + item.Uloga + ";" + ";" + ";" + item.BrojSakupljenihBodova + ";" 
               + item.TipKorisnika.ImeTipa + ";" + item.Obrisan);
            }
            writer.Close();
            stream.Close();
            ViewBag.poruka = "Uspjesno ste promijenili podatke!";
            return View("~/Views/Prodavac/Profil.cshtml", korisnik);
        }
    }
}