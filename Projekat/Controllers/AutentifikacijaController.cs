﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using Projekat.Models;

namespace Projekat.Controllers
{
    public class AutentifikacijaController : Controller
    {

        public ActionResult Registracija()
        {
            //Korisnik korisnik = new Korisnik();
            //Session["korisnik"] = korisnik;
            //return View(korisnik);
            return View("~/Views/Autentifikacija/Registracija.cshtml");
        }
       
        //=============================================================
        [HttpPost]
        public ActionResult Registracija(Korisnik korisnik)
        {
            List<Korisnik> listaKorisnika = Data.ReadUsers("~/App_Data/korisnici.txt");

            if (korisnik.KorisnickoIme == null || korisnik.Lozinka == null || korisnik.Ime == null || korisnik.Prezime == null || korisnik.DatumRodjenja == null)
            {
                ViewBag.Message = "Morate popuniti sva polja!";
                return View(); 
            }
            else
            {
                foreach (Korisnik item in listaKorisnika)
                {
                    if (item.KorisnickoIme == korisnik.KorisnickoIme && item.Obrisan == false)
                    {
                        ViewBag.Message = $"Korisnik sa korisnickim imenom '{korisnik.KorisnickoIme}' vec postoji u bazi!\n";
                        return View();
                    }
                }
            }

            if (!Regex.Match(korisnik.Ime, "^[A-Z][a-zA-Z]*$").Success)
            {
                ViewBag.Message = "Ime mora zapoceti velikim slovom! Nisu dozvoljeni brojevi!";
                return View();
            }
            else if (!Regex.Match(korisnik.Prezime, "^[A-Z][a-zA-Z]*$").Success)
            {
                ViewBag.Message = "Prezime mora zapoceti velikim slovom! Nisu dozvoljeni brojevi!";
                return View();
            }

            Session["korisnik"] = korisnik;
            listaKorisnika.Add(korisnik);
            Data.SacuvajKorisnika(korisnik);
            return RedirectToAction("Prijava", "Autentifikacija"); 
        }

        // GET: Autentifikacija
        public ActionResult Prijava()
        {
            return View();
        }
        
        [HttpPost]
        public ActionResult Prijava(string korisnickoIme, string lozinka)
        {
            List<Korisnik> listaKorisnika=Data.ReadUsers("~/App_Data/korisnici.txt");

            if(korisnickoIme=="" || lozinka == "")
            {
                ViewBag.Message = "Morate popuniti sva polja!";
                return View();
            }

            foreach(Korisnik item in listaKorisnika)
            {
                if(item.KorisnickoIme.Equals(korisnickoIme) && item.Lozinka.Equals(lozinka))
                {
                    if(item.Obrisan == false) { 
                        if (item.Uloga == UlogaTip.Administrator)
                        {
                            Session["korisnik"] = item;
                            ViewBag.Korisnik = item;
                            return RedirectToAction("Index", "Administrator"); //OVO KASNIJE PROMJENITI!!!!!
                        }
                        else if (item.Uloga == UlogaTip.Prodavac)
                        {
                            Session["korisnik"] = item;
                            ViewBag.Korisnik = item;
                            return RedirectToAction("ListaManifestacija", "Prodavac"); //OVO KASNIJE PROMJENITI!!!!!
                        }
                        else
                        {
                            Session["korisnik"] = item;
                            ViewBag.Korisnik = item;
                            return RedirectToAction("Index", "Kupac"); //OVO KASNIJE PROMJENITI!!!!!

                        }
                    }
                }
            }

            ViewBag.Message = "Neispravno korisnicko ime i/ili lozinka!";
            return View();
        }


        public ActionResult Odjava()
        {
            Korisnik korisnik = (Korisnik)Session["korisnik"];
            Session["korisnik"] = null;
            Session["korpa"] = null;
            return View("~/Views/Autentifikacija/Prijava.cshtml");
        }
    }

}