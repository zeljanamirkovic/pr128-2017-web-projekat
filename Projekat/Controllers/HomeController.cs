﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Projekat.Models;

namespace Projekat.Controllers
{
   
    public class HomeController : Controller
    {
        //public static List<Manifestacija> manifestacije = Data.ProcitajManifestacije("~/App_Data/manifestacije.txt");
       // static bool AdminPozvaoMetodu = false;
        //static bool ProdavacPozvaoMetodu = false;
        public ActionResult Index()
        {
            Korisnik korisnik = (Korisnik)Session["korisnik"];

            List<Manifestacija> listaManifestacija = Data.ProcitajManifestacije("~/App_Data/manifestacije.txt");
            List<Manifestacija> lista = new List<Manifestacija>();

            List<Manifestacija> datumV = new List<Manifestacija>();
            List<Manifestacija> sortiranoV = new List<Manifestacija>();

            List<Manifestacija> datumM = new List<Manifestacija>();
            List<Manifestacija> sortiranoM = new List<Manifestacija>();

            for(int i=0; i<listaManifestacija.Count; i++)
            {
                if(listaManifestacija[i].Obrisana == false && listaManifestacija[i].DatumVrijemeOdrzavanja>=DateTime.Now)
                {
                    datumV.Add(listaManifestacija[i]);
                }else if(listaManifestacija[i].Obrisana == false && listaManifestacija[i].DatumVrijemeOdrzavanja < DateTime.Now)
                {
                    datumM.Add(listaManifestacija[i]);
                }
            }

            sortiranoV = datumV.OrderBy(sort => sort.DatumVrijemeOdrzavanja).ToList();
            lista = sortiranoV;

            sortiranoM = datumM.OrderByDescending(sort => sort.DatumVrijemeOdrzavanja).ToList();

            for(int i=0; i<sortiranoM.Count; i++)
            {
                lista.Add(sortiranoM[i]);
            }

            ViewBag.manifestacije = lista;
            return View("~/Views/Home/Index.cshtml", lista);
        }
//==========================================================================


        public ActionResult Filtriranje(string filter)
        {
            //AdminPozvaoMetodu = false;
            // ProdavacPozvaoMetodu = false;
            List<Manifestacija> manifestacije = Data.ProcitajManifestacije("~/App_Data/manifestacije.txt");
            List<Manifestacija> manifes = new List<Manifestacija>();


            for (int i = 0; i < manifestacije.Count; i++)
            {
                if (!manifestacije[i].Obrisana)
                {
                    manifes.Add(manifestacije[i]);
                }
            }

            manifestacije = manifes;
            List<Manifestacija> filtrirano = new List<Manifestacija>();
            Korisnik korisnik = (Korisnik)Session["korisnik"];
            if (filter == "Koncert")
            {
                for (int i = 0; i < manifestacije.Count; i++)
                {
                    if (manifestacije[i].TipManifestacije == ManifestacijaTip.Koncert)
                    {
                        filtrirano.Add(manifestacije[i]);
                    }
                }
            }
            else if (filter == "Pozoriste")
            {
                for (int i = 0; i < manifestacije.Count; i++)
                {
                    if (manifestacije[i].TipManifestacije == ManifestacijaTip.Pozoriste)
                    {
                        filtrirano.Add(manifestacije[i]);
                    }
                }
            }
            else if (filter == "Festival")
            {
                for (int i = 0; i < manifestacije.Count; i++)
                {
                    if (manifestacije[i].TipManifestacije == ManifestacijaTip.Festival)
                    {
                        filtrirano.Add(manifestacije[i]);
                    }
                }
            }
            else if (filter == "Nerasprodato")
            {
                for (int i = 0; i < manifestacije.Count; i++)
                {
                    if (manifestacije[i].BrojMjesta != manifestacije[i].BrojRezervisanihKarata)
                    {
                        filtrirano.Add(manifestacije[i]);
                    }
                }
            }

            ViewBag.manifestacije = filtrirano;
            manifestacije = filtrirano;
            List<Manifestacija> logovaniK = new List<Manifestacija>();
            List<Manifestacija> prodavac = new List<Manifestacija>();
            for (int i = 0; i < filtrirano.Count; i++)
            {
                if (filtrirano[i].BrojMjesta > 0 && filtrirano[i].Status == StatusTip.Aktivno)
                {
                    logovaniK.Add(filtrirano[i]);
                }
                if (korisnik != null && filtrirano[i].KorisnickoIme == korisnik.KorisnickoIme && korisnik.Obrisan == false)
                {
                    prodavac.Add(filtrirano[i]);
                }
            }
            if (korisnik != null && korisnik.Uloga == UlogaTip.Kupac)
            {
                ViewBag.Korisnik = korisnik;
                return View("~/Views/Kupac/Index.cshtml", logovaniK);
            }
            else if (korisnik != null && korisnik.Uloga == UlogaTip.Prodavac)
            {
                //ProdavacPozvaoMetodu = true;
                return View("~/Views/Prodavac/ListaManifestacija.cshtml", prodavac);
            }
            else if (korisnik != null && korisnik.Uloga == UlogaTip.Administrator)
            {
                ViewBag.manifestacije = filtrirano;
                //AdminPozvaoMetodu = true;
                return View("~/Views/Administrator/ListaSvihManfestacija.cshtml", filtrirano);
            }
            return View("~/Views/Home/Index.cshtml", filtrirano);
        }

        public ActionResult PonistiAkciju()
        {
            List<Manifestacija> manifestacije = Data.ProcitajManifestacije("~/App_Data/manifestacije.txt");
            return RedirectToAction("Index", "Home");

        }
//============================================================

        public ActionResult PretragaManifestacija(string nazivManifestacije, string mjestoOdrzavanja,
            DateTime? minDatum, DateTime? maxDatum, string lokacija, int? minCijena, int? maxCijena)
        {
           // AdminPozvaoMetodu = false;
            //ProdavacPozvaoMetodu = false;
            List<Manifestacija> pretrazeneManifestacije = new List<Manifestacija>();
            List<Manifestacija> manifestacije = Data.ProcitajManifestacije("~/App_Data/manifestacije.txt");

            if (nazivManifestacije!="" && nazivManifestacije != null)
            {
                manifestacije = manifestacije.Where(m => m.Obrisana == false && m.Naziv.ToUpper().Contains(nazivManifestacije.ToUpper())).ToList();
            }

            if (mjestoOdrzavanja!="" && mjestoOdrzavanja!=null)
            {
                manifestacije = manifestacije.Where(m => m.Obrisana == false && m.MjestoOdrzavanja.ToUpper().Contains(mjestoOdrzavanja.ToUpper())).ToList();
            }

            if (minDatum != null && maxDatum == null)
            {
                foreach (Manifestacija manifestacija in manifestacije)
                {
                    if (manifestacija.Obrisana == false && manifestacija.DatumVrijemeOdrzavanja.Date == minDatum)
                    {
                        pretrazeneManifestacije.Add(manifestacija);

                    }
                }
                manifestacije = pretrazeneManifestacije;
            }
            else if (minDatum == null && maxDatum != null)
            {
                foreach (Manifestacija manifestacija in manifestacije)
                {
                    if (manifestacija.Obrisana == false && manifestacija.DatumVrijemeOdrzavanja.Date == maxDatum)
                    {
                        pretrazeneManifestacije.Add(manifestacija);

                    }
                }
                manifestacije = pretrazeneManifestacije;
            }
            else if (minDatum != null && maxDatum != null)
            {
                if(minDatum > maxDatum)
                {
                    foreach (Manifestacija manifestacija in manifestacije)
                    {

                        if (manifestacija.Obrisana == false && manifestacija.DatumVrijemeOdrzavanja.Date >= maxDatum && manifestacija.DatumVrijemeOdrzavanja.Date <= minDatum)
                        {
                            pretrazeneManifestacije.Add(manifestacija);

                        }
                    }
                }
                else
                {
                    foreach (Manifestacija manifestacija in manifestacije)
                    {

                        if (manifestacija.Obrisana == false && manifestacija.DatumVrijemeOdrzavanja.Date >= minDatum && manifestacija.DatumVrijemeOdrzavanja.Date <= maxDatum)
                        {
                            pretrazeneManifestacije.Add(manifestacija);

                        }
                    }
                }
               
                manifestacije = pretrazeneManifestacije;
            }

            if (lokacija!="" && lokacija!=null)
            {
                manifestacije = manifestacije.Where(m => m.Obrisana == false && m.Lokacija.ToUpper().Contains(lokacija.ToUpper())).ToList();
                
            }

           
            if(minCijena!= null && maxCijena == null)
            {
                pretrazeneManifestacije = new List<Manifestacija>();
                foreach (Manifestacija manifestacija in manifestacije)
                {
                    if (manifestacija.Obrisana == false && manifestacija.CijenaRegularKarte == minCijena)
                    {
                        pretrazeneManifestacije.Add(manifestacija);

                    }
                }
                manifestacije = pretrazeneManifestacije;
            }
            else if (minCijena == null && maxCijena != null)
            {
                pretrazeneManifestacije = new List<Manifestacija>();
                foreach (Manifestacija manifestacija in manifestacije)
                {
                    if (manifestacija.Obrisana == false && manifestacija.CijenaRegularKarte == maxCijena)
                    {
                        pretrazeneManifestacije.Add(manifestacija);

                    }
                }
                manifestacije = pretrazeneManifestacije;
            }
            else if (minCijena != null && maxCijena != null)
            {
                pretrazeneManifestacije = new List<Manifestacija>();
                if (maxCijena >= minCijena)
                {
                    foreach (Manifestacija manifestacija in manifestacije)
                    {

                        if (manifestacija.Obrisana == false && manifestacija.CijenaRegularKarte >= minCijena && manifestacija.CijenaRegularKarte <= maxCijena)
                        {
                            pretrazeneManifestacije.Add(manifestacija);

                        }
                    }
                }
                else
                {
                    foreach (Manifestacija manifestacija in manifestacije)
                    {

                        if (manifestacija.Obrisana == false && manifestacija.CijenaRegularKarte >= maxCijena && manifestacija.CijenaRegularKarte <= minCijena)
                        {
                            pretrazeneManifestacije.Add(manifestacija);

                        }
                    }
                }
                manifestacije = pretrazeneManifestacije;
            }


            ViewBag.manifestacije = manifestacije;

            Korisnik korisnik = (Korisnik)Session["korisnik"];
            List<Manifestacija> logovaniK = new List<Manifestacija>();
            List<Manifestacija> prodavac = new List<Manifestacija>();
            foreach (Manifestacija m in manifestacije)
            {
                if(m.BrojMjesta>0 && m.Status == StatusTip.Aktivno)
                {
                    logovaniK.Add(m);
                }

                if (korisnik!=null && m.KorisnickoIme == korisnik.KorisnickoIme)
                {
                    prodavac.Add(m);
                }
            }
            if( korisnik!=null && korisnik.Uloga == UlogaTip.Kupac)
            {
                ViewBag.Korisnik = korisnik;
                return View("~/Views/Kupac/Index.cshtml", logovaniK);
            }
            else if(korisnik!=null && korisnik.Uloga == UlogaTip.Prodavac)
            {
               // ProdavacPozvaoMetodu = true;
                return View("~/Views/Prodavac/ListaManifestacija.cshtml", prodavac);
            }
            else if( korisnik!=null && korisnik.Uloga == UlogaTip.Administrator)
            {
                ViewBag.manifestacije = manifestacije;
                //AdminPozvaoMetodu = true;
                return View("~/Views/Administrator/ListaSvihManfestacija.cshtml", manifestacije);
            }

            return View("~/Views/Home/Index.cshtml", manifestacije);
        }


        //-------------------------------------------SORTIRANJE------------------------------------------------------------------------------------
        public ActionResult Sortiranje(string sortiranje, string nacin)
        {
           // AdminPozvaoMetodu = false;
            //ProdavacPozvaoMetodu = false;
            List<Manifestacija> manifestacije = Data.ProcitajManifestacije("~/App_Data/manifestacije.txt");
            List<Manifestacija> sortirane = new List<Manifestacija>();
            List<Manifestacija> manifes = new List<Manifestacija>();

            for(int i=0; i<manifestacije.Count; i++)
            { 
                if (!manifestacije[i].Obrisana)
                {
                    manifes.Add(manifestacije[i]);
                }
            }

            manifestacije = manifes;

            if (sortiranje == "Naziv_manifestacije")
            {
                if (nacin == "Opadajuce")
                {
                    sortirane = manifestacije.OrderByDescending(sort => sort.Naziv).ToList();
                    
                }
                else if (nacin == "Rastuce")
                {
                    sortirane = manifestacije.OrderBy(sort => sort.Naziv).ToList();
                }
                else
                {
                    manifestacije = manifestacije.OrderBy(sort => sort.Naziv).ToList();  
                }
            }
            else if (sortiranje == "Datum_vrijeme_odrzavanja")
            {
                if (nacin == "Opadajuce")
                {
                    sortirane = manifestacije.OrderByDescending(sort => sort.DatumVrijemeOdrzavanja).ToList();                    
                }
                else if (nacin == "Rastuce")
                {
                    sortirane = manifestacije.OrderBy(sort => sort.DatumVrijemeOdrzavanja).ToList();          
                }
                else
                {
                    sortirane = manifestacije.OrderBy(sort => sort.DatumVrijemeOdrzavanja).ToList();
                }
            }
            else if (sortiranje == "Cijena_karte")
            {
                if (nacin == "Opadajuce")
                {
                    sortirane = manifestacije.OrderByDescending(sort => sort.CijenaRegularKarte).ToList();
                }
                else if (nacin == "Rastuce")
                {
                    sortirane = manifestacije.OrderBy(sort => sort.CijenaRegularKarte).ToList();
                }
                else
                {
                    sortirane = manifestacije.OrderBy(sort => sort.CijenaRegularKarte).ToList();
                }
            }
            else if (sortiranje == "Mjesto_odrzavanja")
            {
                if (nacin == "Opadajuce")
                {
                    sortirane = manifestacije.OrderByDescending(sort => sort.MjestoOdrzavanja).ToList();
                }
                else if (nacin == "Rastuce")
                {
                    sortirane = manifestacije.OrderBy(sort => sort.MjestoOdrzavanja).ToList();
                }
                else
                {
                    sortirane = manifestacije.OrderBy(sort => sort.MjestoOdrzavanja).ToList();
                }
            }

            ViewBag.manifestacije = sortirane;
            manifestacije = sortirane;
            Korisnik korisnik = (Korisnik)Session["korisnik"];

            
            List<Manifestacija> logovaniK = new List<Manifestacija>();
            List<Manifestacija> prodavac = new List<Manifestacija>();
            foreach (Manifestacija m in sortirane)
            {
                if (m.BrojMjesta > 0 && m.Status == StatusTip.Aktivno)
                {
                    logovaniK.Add(m);
                }
                if (korisnik!=null && m.KorisnickoIme == korisnik.KorisnickoIme && korisnik.Obrisan==false)
                {
                    prodavac.Add(m);
                }
            }
            if (korisnik != null && korisnik.Uloga == UlogaTip.Kupac)
            {
                ViewBag.Korisnik = korisnik;
                return View("~/Views/Kupac/Index.cshtml", logovaniK);
            }
            else if (korisnik != null && korisnik.Uloga == UlogaTip.Prodavac)
            {
                //ProdavacPozvaoMetodu = true;
                return View("~/Views/Prodavac/ListaManifestacija.cshtml", prodavac);
            }
            else if (korisnik != null && korisnik.Uloga == UlogaTip.Administrator)
            {
                ViewBag.manifestacije = sortirane;
                //AdminPozvaoMetodu = true;
                return View("~/Views/Administrator/ListaSvihManfestacija.cshtml", sortirane);
            }
            return View("~/Views/Home/Index.cshtml", sortirane);
        }
    }
}