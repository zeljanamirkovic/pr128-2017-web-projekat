﻿using Projekat.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;

namespace Projekat.Controllers
{
    public class KupacController : Controller
    {

        // GET: Kupac
        public ActionResult Index()
        {
            Korisnik korisnik = (Korisnik)Session["korisnik"];
            if (korisnik == null) 
                return RedirectToAction("Prijava", "Autentifikacija");
//--------------------inicijalizaija korisnicke korpe----------------------------
            List<Karta> korpa = (List<Karta>)Session["korpa"];
            if (korpa == null)
            {
                korpa = new List<Karta>();
                Session["korpa"] = korpa;
            }
//-------------------------------------------------------------------------
            List<Manifestacija> listaManifestacija = Data.ProcitajManifestacije("~/App_Data/manifestacije.txt");
            List<Manifestacija> lista = new List<Manifestacija>();

            List<Manifestacija> datumV = new List<Manifestacija>();
            List<Manifestacija> sortiranoV = new List<Manifestacija>();

            List<Manifestacija> datumM = new List<Manifestacija>();
            List<Manifestacija> sortiranoM = new List<Manifestacija>();

            for (int i = 0; i < listaManifestacija.Count; i++)
            {
                if (listaManifestacija[i].Obrisana == false && listaManifestacija[i].DatumVrijemeOdrzavanja >= DateTime.Now)
                {
                    datumV.Add(listaManifestacija[i]);
                }
                else if (listaManifestacija[i].Obrisana == false && listaManifestacija[i].DatumVrijemeOdrzavanja < DateTime.Now)
                {
                    datumM.Add(listaManifestacija[i]);
                }
            }

            sortiranoV = datumV.OrderBy(sort => sort.DatumVrijemeOdrzavanja).ToList();
            sortiranoM = datumM.OrderByDescending(sort => sort.DatumVrijemeOdrzavanja).ToList();

           for(int i=0; i<sortiranoV.Count; i++)
            {
                if (sortiranoV[i].Status != StatusTip.Neaktivno && sortiranoV[i].BrojMjesta != sortiranoV[i].BrojRezervisanihKarata)
                {
                    lista.Add(sortiranoV[i]);
                }
            }

            for(int i=0; i< sortiranoM.Count; i++)
            {
                if (sortiranoM[i].Status != StatusTip.Neaktivno && sortiranoM[i].BrojMjesta != sortiranoM[i].BrojRezervisanihKarata)
                {
                    lista.Add(sortiranoM[i]);
                }
            }
          
            ViewBag.korisnik = korisnik;
            ViewBag.manifestacije = lista;
            return View("~/Views/Kupac/Index.cshtml", lista);


        }


        [HttpPost]
        public ActionResult PregledManifestacije(string datumVrijemeOdrzavanja, string lokacija)
        {
            List<Manifestacija> listaManifestacija = Data.ProcitajManifestacije("~/App_Data/manifestacije.txt");
            Manifestacija manifestacija = new Manifestacija();
            for (int i = 0; i < listaManifestacija.Count; i++)
            {
                if (listaManifestacija[i].Obrisana == false && listaManifestacija[i].Lokacija == lokacija && listaManifestacija[i].DatumVrijemeOdrzavanja.ToString() == datumVrijemeOdrzavanja)
                {
                    manifestacija = listaManifestacija[i];
                    break;
                }
            }
            ViewBag.manifestacija = manifestacija;
            return View("~/Views/Kupac/Detalji.cshtml", manifestacija);
        }


        [HttpPost]
        public ActionResult RezervisiKartu(string datumVrijemeOdrzavanja, string lokacija, int brojKarata, string tip)
        {
            List<Manifestacija> listaManifestacija = Data.ProcitajManifestacije("~/App_Data/manifestacije.txt");
            Manifestacija manifestacija = new Manifestacija();
            Korisnik korisnik = (Korisnik)Session["korisnik"];
            double cijena = 0;

            for (int i = 0; i < listaManifestacija.Count; i++)
            {
                if (listaManifestacija[i].Obrisana == false && listaManifestacija[i].Lokacija == lokacija && 
                    listaManifestacija[i].DatumVrijemeOdrzavanja.ToString() == datumVrijemeOdrzavanja)
                {
                    manifestacija = listaManifestacija[i];
                    break;
                }
            }

            if (manifestacija.BrojMjesta < brojKarata + manifestacija.BrojRezervisanihKarata)
            {
                ViewBag.Message = $"Nije moguce rezervisati {brojKarata} karata";
                ViewBag.manifestacija = manifestacija;
                return View("~/Views/Kupac/Detalji.cshtml", manifestacija);
            }

            if (tip == TipKarta.REGULAR.ToString())
            {
                if (korisnik.BrojSakupljenihBodova < 1500)
                {
                    cijena += (manifestacija.CijenaRegularKarte * brojKarata);
                }
                else if (korisnik.BrojSakupljenihBodova >= 1500 && korisnik.BrojSakupljenihBodova < 2500)
                {
                    double popust = (manifestacija.CijenaRegularKarte * 0.03);
                    cijena += ((manifestacija.CijenaRegularKarte * brojKarata) - popust);
                } else
                {
                    double popust = (manifestacija.CijenaRegularKarte * 0.05);
                    cijena += ((manifestacija.CijenaRegularKarte * brojKarata) - popust);
                }


            }
            else if (tip == TipKarta.VIP.ToString())
            {
                if (korisnik.BrojSakupljenihBodova < 1500)
                {
                    cijena += (manifestacija.CijenaRegularKarte * brojKarata * 4);
                }
                else if (korisnik.BrojSakupljenihBodova >= 1500 && korisnik.BrojSakupljenihBodova < 2500)
                {
                    double popust = (manifestacija.CijenaRegularKarte * 4 * 0.03);
                    cijena += ((manifestacija.CijenaRegularKarte * brojKarata * 4) - popust);
                }
                else
                {
                    double popust = (manifestacija.CijenaRegularKarte * 4 * 0.05);
                    cijena += ((manifestacija.CijenaRegularKarte * brojKarata * 4) - popust);
                }
            }
            else if (tip == TipKarta.FAN_PIT.ToString())
            {
                if (korisnik.BrojSakupljenihBodova < 1500) //bronza
                {
                    cijena += (manifestacija.CijenaRegularKarte * brojKarata * 2);
                }
                else if (korisnik.BrojSakupljenihBodova >= 1500 && korisnik.BrojSakupljenihBodova < 2500)//srebro
                {
                    double popust = (manifestacija.CijenaRegularKarte * 2 * 0.03);
                    cijena += ((manifestacija.CijenaRegularKarte * brojKarata * 2) - popust);
                }
                else //zlato
                {
                    double popust = (manifestacija.CijenaRegularKarte * 2 * 0.05);
                    cijena += ((manifestacija.CijenaRegularKarte * brojKarata * 2) - popust);
                }
            }
            else
            {
                ViewBag.Message = "Izarerite tip karte.";
                return View();
            }

            int iznos = (int)cijena;
            ViewBag.manifestacija = manifestacija;
            ViewBag.korisnik = korisnik;
            ViewBag.tip = tip;
            ViewBag.iznos = iznos;
            ViewBag.karte = brojKarata;
            return View("~/Views/Kupac/PotvrdaRezervacije.cshtml", manifestacija);
        }

        [HttpPost]
        public ActionResult PotvrdaRezervacije(string korisnickoIme, int ukuprnoKarata, string tip, string datumVrijemeOdrzavanja, string lokacija, int iznos/*=0.0*/)
        {
            List<Karta> korpa = (List<Karta>)Session["korpa"]; //u slucaju da korisnik nije prijavljen
            if (korpa == null)
                return RedirectToAction("Prijava", "Autentifikacija");

            Korisnik korisnik = (Korisnik)Session["korisnik"];
            List<Manifestacija> listaManifestacija = Data.ProcitajManifestacije("~/App_Data/manifestacije.txt");

            Manifestacija manifes = new Manifestacija();
            for (int i = 0; i < listaManifestacija.Count; i++)
            {
                if (listaManifestacija[i].Obrisana == false && listaManifestacija[i].Lokacija == lokacija && listaManifestacija[i].DatumVrijemeOdrzavanja.ToString() == datumVrijemeOdrzavanja)
                {
                    manifes = listaManifestacija[i];
                    break;
                }
            }
//---------------------------------upis rezervisanih karata------------------------------------------------------------------ 
            List<Karta> rezervisaneKarte = new List<Karta>();
            int bodovi = 0;
            double ukupnaCijena = iznos / ukuprnoKarata;
            TipKarta tipKarte;
            string kupac = korisnik.Ime + " " + korisnik.Prezime;

            if (tip == TipKarta.REGULAR.ToString())
            {
                tipKarte = TipKarta.REGULAR;
            }
            else if (tip == TipKarta.VIP.ToString())
            {
                tipKarte = TipKarta.VIP;
            }
            else
            {
                tipKarte = TipKarta.FAN_PIT;
            }

            Random random = new Random();
            for (int i = 0; i < ukuprnoKarata; i++)
            {
                string idKarte = korisnik.KorisnickoIme + "." + random.Next(1, 100000);
                //datum rezervacije
                Karta karta = new Karta(idKarte, manifes, DateTime.Now, ukupnaCijena, kupac, StatusKarta.Rezervisana, tipKarte);
                rezervisaneKarte.Add(karta);

                bodovi += (int)(ukupnaCijena / 1000 * 133);



            }
            Data.SacuvajKarte(rezervisaneKarte);
//-----------------------------------------------------------------------------------------------
//-------------------------------promjena bodova--------------------------------
            List<Korisnik> listaKorisnika = Data.ReadUsers("~/App_Data/korisnici.txt");

            if (korisnik.TipKorisnika.ImeTipa == TKImeTip.Bronzani)
            {
                if (korisnik.BrojSakupljenihBodova + bodovi < 1500)
                {
                    korisnik.BrojSakupljenihBodova += bodovi;
                }
                else if (korisnik.BrojSakupljenihBodova + bodovi >= 1500 && korisnik.BrojSakupljenihBodova + bodovi < 2500)
                {

                    listaKorisnika.Find(k => k.KorisnickoIme.Equals(korisnik.KorisnickoIme) && k.Obrisan == false).TipKorisnika.ImeTipa = TKImeTip.Srebrni; //da sacuvam promjenu bodova na korisniku
                    korisnik.BrojSakupljenihBodova += bodovi;

                }
                else
                {

                    listaKorisnika.Find(k => k.KorisnickoIme.Equals(korisnik.KorisnickoIme) && k.Obrisan == false).TipKorisnika.ImeTipa = TKImeTip.Zlatni;
                    korisnik.BrojSakupljenihBodova += bodovi;
                }
            }
            else if (korisnik.TipKorisnika.ImeTipa == TKImeTip.Srebrni)
            {
                if (korisnik.BrojSakupljenihBodova + bodovi >= 1500 && korisnik.BrojSakupljenihBodova + bodovi < 2500)
                {
                    korisnik.BrojSakupljenihBodova += bodovi;
                }
                else if (korisnik.BrojSakupljenihBodova + bodovi > 2500)
                {
                    listaKorisnika.Find(k => k.KorisnickoIme.Equals(korisnik.KorisnickoIme) && k.Obrisan == false).TipKorisnika.ImeTipa = TKImeTip.Zlatni;
                    korisnik.BrojSakupljenihBodova += bodovi;
                }
            }
            else if (korisnik.TipKorisnika.ImeTipa == TKImeTip.Zlatni)
            {
                korisnik.BrojSakupljenihBodova += bodovi;
            }

            for (int i = 0; i < listaKorisnika.Count; i++) //da promjenu upisem i u listu korisnika
            {
                if (listaKorisnika[i].KorisnickoIme == korisnik.KorisnickoIme && listaKorisnika[i].Obrisan == false)
                {
                    listaKorisnika[i].BrojSakupljenihBodova += bodovi;
                    break;
                }
            }

            string path1 = HostingEnvironment.MapPath("~/App_Data/korisnici.txt");
            FileStream stream1 = new FileStream(path1, FileMode.Create);
            StreamWriter sw1 = new StreamWriter(stream1);

            foreach (Korisnik item in listaKorisnika)
            {
                sw1.WriteLine(item.KorisnickoIme + ";" + item.Lozinka + ";" + item.Ime + ";" +
                   item.Prezime + ";" + item.Pol + ";" + item.DatumRodjenja + ";" + item.Uloga + ";" + ";" + ";" + item.BrojSakupljenihBodova + ";" + item.TipKorisnika.ImeTipa + ";" + item.Obrisan);

            }
            sw1.Close();
            stream1.Close();
//----------------------------------------------------------------------------

 //---------------------povecanje broja rezervisanih karata-----------------------------------
            for (int i = 0; i < listaManifestacija.Count; i++)
            {
                if (listaManifestacija[i].Obrisana == false && listaManifestacija[i].Lokacija == lokacija && listaManifestacija[i].DatumVrijemeOdrzavanja.ToString() == datumVrijemeOdrzavanja)
                {
                    listaManifestacija[i].BrojRezervisanihKarata +=ukuprnoKarata;
                    break;
                }
            }
            string putanja = HostingEnvironment.MapPath("~/App_Data/manifestacije.txt");
            FileStream stream = new FileStream(putanja, FileMode.Create);
            StreamWriter writer = new StreamWriter(stream);

            foreach (Manifestacija item in listaManifestacija)
            {
                writer.WriteLine(item.Naziv + ";" + item.TipManifestacije + ";" + item.BrojMjesta + ";"
                + item.DatumVrijemeOdrzavanja + ";" + item.CijenaRegularKarte + ";" + item.Status + ";" + item.MjestoOdrzavanja + ";"
                + item.Lokacija + ";" + item.KorisnickoIme + ";" + item.Ocjena + ";" + item.Obrisana + ";" + item.BrojRezervisanihKarata);      //}

            }
            writer.Close();
            stream.Close();
//------------------------------------------------------------------------------------------------------------------
          

            ViewBag.manifestacije = listaManifestacija;
            return RedirectToAction("Index", "Korpa"); //prikaz kupljenih karata
        }

        [HttpPost]
        public ActionResult OtkaziRezervaciju(string datumVrijemeOdrzavanja, string lokacija, string kartaId)
        {
            List<Manifestacija> listaManifestacija = Data.ProcitajManifestacije("~/App_Data/manifestacije.txt");
            List<Karta> karte = Data.ProcitajKarte("~/App_Data/karte.txt");

            Karta karta = new Karta();
            for (int i = 0; i < karte.Count; i++)
            {
                if (karte[i].JedinstveniIdentifikatorKarte == kartaId)
                {
                    karte[i].Status = StatusKarta.Odustanak;
                    break;
                }
            }

            for (int i = 0; i < karte.Count; i++)
            {
                if (karte[i].JedinstveniIdentifikatorKarte == kartaId)
                {
                    karta = karte[i];
                    break;
                }
            }

            List<Korisnik> listaKorisnika = Data.ReadUsers("~/App_Data/korisnici.txt");
            Korisnik koris = (Korisnik)Session["korisnik"];
            Korisnik korisnik = new Korisnik();

            for (int i = 0; i < listaKorisnika.Count; i++)
            {
                if (listaKorisnika[i].Obrisan == false && listaKorisnika[i].KorisnickoIme == koris.KorisnickoIme)
                {
                    korisnik = listaKorisnika[i];
                    break;
                }
            }

            string path = HostingEnvironment.MapPath("~/App_Data/karte.txt");
            FileStream stream = new FileStream(path, FileMode.Create);
            StreamWriter sw = new StreamWriter(stream);

            foreach (Karta item in karte)
            {
                sw.WriteLine(item.JedinstveniIdentifikatorKarte + ";" + item.Manifestacija.Lokacija + ";" +
               item.Manifestacija.DatumVrijemeOdrzavanja + ";" + item.DatumVrijemeManifestacije + ";" + item.Cijena + ";" + item.Kupac + ";" + item.Status + ";" + item.Tip);
            }

            sw.Close();
            stream.Close();



            //-----------------------------------povecam broj dostupnih mjesta za 1----------------------------------------------------------

            for (int i = 0; i < listaManifestacija.Count; i++)
            {
                if (listaManifestacija[i].Obrisana == false && listaManifestacija[i].Lokacija == lokacija && listaManifestacija[i].DatumVrijemeOdrzavanja.ToString() == datumVrijemeOdrzavanja)
                {
                    listaManifestacija[i].BrojRezervisanihKarata -= 1;
                    break;
                }
            }
            string path1 = HostingEnvironment.MapPath("~/App_Data/manifestacije.txt");
            FileStream stream1 = new FileStream(path1, FileMode.Create);
            StreamWriter sw1 = new StreamWriter(stream1);

            foreach (Manifestacija item in listaManifestacija)
            {
                sw1.WriteLine(item.Naziv + ";" + item.TipManifestacije + ";" + item.BrojMjesta + ";"
                + item.DatumVrijemeOdrzavanja + ";" + item.CijenaRegularKarte + ";" + item.Status + ";" + item.MjestoOdrzavanja + ";" + item.Lokacija + ";"
                + item.KorisnickoIme + ";" + item.Ocjena + ";" + item.Obrisana + ";" + item.BrojRezervisanihKarata);      //}

            }
            sw1.Close();
            stream1.Close();
            //------------------------------------------------------------------------------------------------------------------
            //--------------------------OVDJE URADITITI CUVANJE PROMJENE BODOVA NA KLIJENTU-------------------

            double brojIzgubljenihBodova = karta.Cijena / 1000 * 133 * 4;



            if (korisnik.BrojSakupljenihBodova - brojIzgubljenihBodova <= 0)
            {
                listaKorisnika.Find(k => k.KorisnickoIme.Equals(korisnik.KorisnickoIme) && k.Obrisan == false).BrojSakupljenihBodova = 0;
                listaKorisnika.Find(k => k.KorisnickoIme.Equals(korisnik.KorisnickoIme) && k.Obrisan == false).TipKorisnika.ImeTipa = TKImeTip.Bronzani;
            }
            else
            {
                listaKorisnika.Find(k => k.KorisnickoIme.Equals(korisnik.KorisnickoIme) && k.Obrisan == false).BrojSakupljenihBodova -= (int)brojIzgubljenihBodova; //da sacuvam promjenu bodova na korisniku

                if (listaKorisnika.Find(k => k.KorisnickoIme.Equals(korisnik.KorisnickoIme) && k.Obrisan == false).BrojSakupljenihBodova >= 1500 && listaKorisnika.Find(k => k.KorisnickoIme.Equals(korisnik.KorisnickoIme) && k.Obrisan == false).BrojSakupljenihBodova < 2500) //kad radim sa korisnik.BrojSakupljenihBodova onda mi prikaze stare bodove, tj. ne prikaze novo stanje nakon oduzimanja
                {
                    listaKorisnika.Find(k => k.KorisnickoIme.Equals(korisnik.KorisnickoIme) && k.Obrisan == false).TipKorisnika.ImeTipa = TKImeTip.Srebrni;
                }
                else if (listaKorisnika.Find(k => k.KorisnickoIme.Equals(korisnik.KorisnickoIme) && k.Obrisan == false).BrojSakupljenihBodova > 2500)
                {
                    listaKorisnika.Find(k => k.KorisnickoIme.Equals(korisnik.KorisnickoIme) && k.Obrisan == false).TipKorisnika.ImeTipa = TKImeTip.Zlatni;

                }
                else
                {
                    listaKorisnika.Find(k => k.KorisnickoIme.Equals(korisnik.KorisnickoIme) && k.Obrisan == false).TipKorisnika.ImeTipa = TKImeTip.Bronzani;
                }
            }


            string path2 = HostingEnvironment.MapPath("~/App_Data/korisnici.txt");
            FileStream stream2 = new FileStream(path2, FileMode.Create);
            StreamWriter sw2 = new StreamWriter(stream2);

            foreach (Korisnik item in listaKorisnika)
            {
                sw2.WriteLine(item.KorisnickoIme + ";" + item.Lozinka + ";" + item.Ime + ";" +
                   item.Prezime + ";" + item.Pol + ";" + item.DatumRodjenja + ";" + item.Uloga + ";" + ";" + ";" + item.BrojSakupljenihBodova + ";" + item.TipKorisnika.ImeTipa + ";" + item.Obrisan);

            }
            sw2.Close();
            stream2.Close();
            return RedirectToAction("Index", "Korpa");
        }

        public ActionResult Profil()
        {
            Korisnik korisnik = (Korisnik)Session["korisnik"];

            List<Korisnik> korisnici = Data.ReadUsers("~/App_Data/korisnici.txt"); //ne prikaze mi modifikovane podatke kad ponovo udjem na profil
            Korisnik izmijenjen = new Korisnik();
            foreach (Korisnik k in korisnici)
            {
                if (k.KorisnickoIme == korisnik.KorisnickoIme && k.Obrisan == false)
                    izmijenjen = k;
            }
            return View("~/Views/Kupac/Profil.cshtml", izmijenjen);

        }

        [HttpPost]
        public ActionResult Profil(Korisnik korisnik)
        {
            Korisnik k = (Korisnik)Session["korisnik"];
            List<Korisnik> listaKorisnika = Data.ReadUsers("~/App_Data/korisnici.txt");

            if (korisnik.KorisnickoIme == null || korisnik.Lozinka == null || korisnik.Ime == null || korisnik.Prezime == null)
            {
                ViewBag.Message = $"Ne sme biti prazno polje";
                // return RedirectToAction("Profil", "Kupac");
                return View();
            }

            listaKorisnika.Find(kor => kor.KorisnickoIme.Equals(k.KorisnickoIme) && kor.Obrisan == false).Lozinka = korisnik.Lozinka;
            listaKorisnika.Find(kor => kor.KorisnickoIme.Equals(k.KorisnickoIme) && kor.Obrisan == false).Ime = korisnik.Ime;
            listaKorisnika.Find(kor => kor.KorisnickoIme.Equals(k.KorisnickoIme) && kor.Obrisan == false).Prezime = korisnik.Prezime;
            listaKorisnika.Find(kor => kor.KorisnickoIme.Equals(k.KorisnickoIme) && kor.Obrisan == false).Pol = korisnik.Pol;
            listaKorisnika.Find(kor => kor.KorisnickoIme.Equals(k.KorisnickoIme) && kor.Obrisan == false).DatumRodjenja = korisnik.DatumRodjenja;

            string path = HostingEnvironment.MapPath("~/App_Data/korisnici.txt");
            FileStream stream = new FileStream(path, FileMode.Create);
            StreamWriter sw = new StreamWriter(stream);
            foreach (Korisnik item in listaKorisnika)
            {

                sw.WriteLine(item.KorisnickoIme + ";" + item.Lozinka + ";" + item.Ime + ";" +
               item.Prezime + ";" + item.Pol + ";" + item.DatumRodjenja + ";" + item.Uloga + ";" + ";" + ";" + item.BrojSakupljenihBodova + ";" + item.TipKorisnika.ImeTipa + ";" + item.Obrisan);
            }
            sw.Close();
            stream.Close();
            ViewBag.poruka = "Uspjesno ste promijenili podatke!";

            return View("~/Views/Kupac/Profil.cshtml", korisnik);
        }



        public ActionResult OstaviKomentar(string lokacija, string datumVrijemeOdrzavanja)
        {
            List<Manifestacija> listaManifestacija = Data.ProcitajManifestacije("~/App_Data/manifestacije.txt");
            Manifestacija manifestacija = new Manifestacija();
            for (int i = 0; i < listaManifestacija.Count; i++)
            {
                if(listaManifestacija[i].Lokacija==lokacija && listaManifestacija[i].DatumVrijemeOdrzavanja.ToString() == datumVrijemeOdrzavanja && listaManifestacija[i].Obrisana==false)
                {
                    manifestacija = listaManifestacija[i];
                }
            }
            ViewBag.manifestacija = manifestacija;
            return View("~/Views/Kupac/Komentar.cshtml", manifestacija);
        }


        [HttpPost]
        public ActionResult OstaviKomentar(string lokacija, string datumVrijemeOdrzavanja, string komentar,int ocjena)
        {
            List<Manifestacija> listaManifestacija = Data.ProcitajManifestacije("~/App_Data/manifestacije.txt");
            Manifestacija trenutna = new Manifestacija();
            for (int i = 0; i < listaManifestacija.Count; i++)
            {
                if (listaManifestacija[i].Lokacija == lokacija && listaManifestacija[i].DatumVrijemeOdrzavanja.ToString() == datumVrijemeOdrzavanja && listaManifestacija[i].Obrisana == false)
                {
                    trenutna = listaManifestacija[i];
                }
            }

            Korisnik korisnik = (Korisnik)Session["korisnik"];
            Random random = new Random();
            int id = random.Next(1, 100000);
           Komentar k = new Komentar(korisnik.KorisnickoIme, trenutna, komentar, ocjena, StatusTip.Neaktivno, id);
           Data.SacuvajKomentar(k);

            return RedirectToAction("Index", "Korpa");

            
        }

        [HttpPost]
        public ActionResult ListaKomentara(string lokacija, string datumVrijemeOdrzavanja)
        {
            List<Komentar> listaKomentara = Data.ProcitajKomentare("~/App_Data/komentari.txt");
            List<Manifestacija> listaManifestacija = Data.ProcitajManifestacije("~/App_Data/manifestacije.txt");
            List<Komentar> lista = new List<Komentar>();

            for (int i = 0; i < listaKomentara.Count; i++)
            {
                if (listaKomentara[i].ManifestacijaKomentar.Lokacija == lokacija && listaKomentara[i].ManifestacijaKomentar.DatumVrijemeOdrzavanja.ToString() == datumVrijemeOdrzavanja)
                {
                    lista.Add(listaKomentara[i]);
                }
            }

            ViewBag.komentari = lista;
            return View("~/Views/Kupac/ListaKomentara.cshtml", lista);
        }

    }
}