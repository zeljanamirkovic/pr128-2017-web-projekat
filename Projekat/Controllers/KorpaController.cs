﻿using Projekat.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Projekat.Controllers
{
    public class KorpaController : Controller
    {
        // GET: Korpa
        public ActionResult Index()//prikaz rezervisanih karata
        {
            List<Karta> korpa = (List<Karta>)Session["korpa"];
            if (korpa == null)
                return RedirectToAction("Prijava", "Autentifikacija");

            List<Karta> listaKarata = Data.ProcitajKarte("~/App_Data/karte.txt");
            List<Karta> rezultat = new List<Karta>();
            Korisnik korisnik = (Korisnik)Session["korisnik"];

           /*  string datumManifestacije = "";
             string datumTrenutni = DateTime.Now.ToString();
           
    */
            foreach (Karta item in listaKarata)
            {
                string line = item.JedinstveniIdentifikatorKarte;
                string[] podaci = line.Split('.');
                if (podaci[0] == korisnik.KorisnickoIme && item.Status == StatusKarta.Rezervisana && korisnik.Obrisan == false)
                {
                    rezultat.Add(item);                  
                }
            }

            ViewBag.lista = rezultat;
            return View("~/Views/Korpa/Index.cshtml", rezultat);
        }


        public ActionResult SortiranjeKarata(string sortiranje, string nacin)
        {
            Korisnik korisnik = (Korisnik)Session["korisnik"];
            List<Karta> listaKarata = Data.ProcitajKarte("~/App_Data/karte.txt");
            List<Karta> sortirane = new List<Karta>();

            List<Karta> rezultat = new List<Karta>();

            if (sortiranje == "Naziv_manifestacije")
            {
                if (nacin == "Opadajuce")
                {
                    sortirane = listaKarata.OrderByDescending(sort => sort.Manifestacija.Naziv).ToList();
                    foreach (Karta item in sortirane)
                    {
                        string line = item.JedinstveniIdentifikatorKarte;
                        string[] podaci = line.Split('.');
                        if (item.Status == StatusKarta.Rezervisana && podaci[0] == korisnik.KorisnickoIme)
                        {
                            rezultat.Add(item);
                        }
                    }
                }
                else if (nacin == "Rastuce")
                {
                    sortirane = listaKarata.OrderBy(sort => sort.Manifestacija.Naziv).ToList();
                    foreach (Karta item in sortirane)
                    {
                        string line = item.JedinstveniIdentifikatorKarte;
                        string[] podaci = line.Split('.');
                        if (item.Status == StatusKarta.Rezervisana && podaci[0] == korisnik.KorisnickoIme)
                        {
                            rezultat.Add(item);
                        }
                    }
                }
                else
                {
                    sortirane = listaKarata.OrderBy(sort => sort.Manifestacija.Naziv).ToList();
                    foreach (Karta item in sortirane)
                    {
                        string line = item.JedinstveniIdentifikatorKarte;
                        string[] podaci = line.Split('.');
                        if (item.Status == StatusKarta.Rezervisana && podaci[0] == korisnik.KorisnickoIme)
                        {
                            rezultat.Add(item);
                        }
                    }
                }
            }
            else if (sortiranje == "Cijena")
            {
                if (nacin == "Opadajuce")
                {
                    sortirane = listaKarata.OrderByDescending(sort => sort.Cijena).ToList();
                    foreach (Karta item in sortirane)
                    {
                        string line = item.JedinstveniIdentifikatorKarte;
                        string[] podaci = line.Split('.');
                        if (item.Status == StatusKarta.Rezervisana && podaci[0] == korisnik.KorisnickoIme)
                        {
                            rezultat.Add(item);
                        }
                    }
                }
                else if (nacin == "Rastuce")
                {
                    sortirane = listaKarata.OrderBy(sort => sort.Cijena).ToList();
                    foreach (Karta item in sortirane)
                    {
                        string line = item.JedinstveniIdentifikatorKarte;
                        string[] podaci = line.Split('.');
                        if (item.Status == StatusKarta.Rezervisana && podaci[0] == korisnik.KorisnickoIme)
                        {
                            rezultat.Add(item);
                        }
                    }
                }
                else
                {
                    sortirane = listaKarata.OrderBy(sort => sort.Cijena).ToList();
                    foreach (Karta item in sortirane)
                    {
                        string line = item.JedinstveniIdentifikatorKarte;
                        string[] podaci = line.Split('.');
                        if (item.Status == StatusKarta.Rezervisana && podaci[0] == korisnik.KorisnickoIme)
                        {
                            rezultat.Add(item);
                        }
                    }
                }
            }
            else if (sortiranje == "Datum_odrzavanja")
            {
                if (nacin == "Opadajuce")
                {
                    sortirane = listaKarata.OrderByDescending(sort => sort.Manifestacija.DatumVrijemeOdrzavanja).ToList();
                    foreach (Karta item in sortirane)
                    {
                        string line = item.JedinstveniIdentifikatorKarte;
                        string[] podaci = line.Split('.');
                        if (item.Status == StatusKarta.Rezervisana && podaci[0] == korisnik.KorisnickoIme)
                        {
                            rezultat.Add(item);
                        }
                    }
                }
                else if (nacin == "Rastuce")
                {
                    sortirane = listaKarata.OrderBy(sort => sort.Manifestacija.DatumVrijemeOdrzavanja).ToList();
                    foreach (Karta item in sortirane)
                    {
                        string line = item.JedinstveniIdentifikatorKarte;
                        string[] podaci = line.Split('.');
                        if (item.Status == StatusKarta.Rezervisana && podaci[0] == korisnik.KorisnickoIme)
                        {
                            rezultat.Add(item);
                        }
                    }
                }
                else
                {
                    sortirane = listaKarata.OrderBy(sort => sort.Manifestacija.DatumVrijemeOdrzavanja).ToList();
                    foreach (Karta item in sortirane)   
                    {
                        string line = item.JedinstveniIdentifikatorKarte;
                        string[] podaci = line.Split('.');
                        if (item.Status == StatusKarta.Rezervisana && podaci[0]==korisnik.KorisnickoIme)
                        {
                            rezultat.Add(item);
                        }
                    }
                }
            }

            ViewBag.korisnik = korisnik;
            ViewBag.korisnici = rezultat;
            return View("~/Views/Korpa/Index.cshtml", rezultat);
        }

        public ActionResult FiltriranjeKarata(string filter, string vrijednost)
        {
            List<Karta> listaKarata = Data.ProcitajKarte("~/App_Data/karte.txt");
            List<Karta> listaFiltriranih = new List<Karta>();
            Korisnik korisnik = (Korisnik)Session["korisnik"];

            if (filter == "VIP")
            {
                foreach (Karta item in listaKarata)
                {
                    string line = item.JedinstveniIdentifikatorKarte;
                    string[] podaci = line.Split('.');
                    if (item.Tip == TipKarta.VIP && podaci[0] == korisnik.KorisnickoIme && item.Status == StatusKarta.Rezervisana)
                    {
                        listaFiltriranih.Add(item);
                    }
                }
            }
            else if (filter == "REGULAR")
            {
                foreach (Karta item in listaKarata)
                {
                    string line = item.JedinstveniIdentifikatorKarte;
                    string[] podaci = line.Split('.');
                    if (item.Tip == TipKarta.REGULAR && podaci[0] == korisnik.KorisnickoIme && item.Status == StatusKarta.Rezervisana)
                    {
                        listaFiltriranih.Add(item);
                    }
                }
            }
            else if (filter == "FAN_PIT")
            {
                foreach (Karta item in listaKarata)
                {
                    string line = item.JedinstveniIdentifikatorKarte;
                    string[] podaci = line.Split('.');
                    if (item.Tip == TipKarta.FAN_PIT && podaci[0] == korisnik.KorisnickoIme && item.Status == StatusKarta.Rezervisana)
                    {
                        listaFiltriranih.Add(item);
                    }
                }
            }
            else if (filter == "Rezervisana" )
            {
                foreach (Karta item in listaKarata)
                {
                    string line = item.JedinstveniIdentifikatorKarte;
                    string[] podaci = line.Split('.');
                    if (item.Status == StatusKarta.Rezervisana && podaci[0] == korisnik.KorisnickoIme)
                    {
                        listaFiltriranih.Add(item);
                    }
                }
            }
            else if (filter == "Otkazane")
            {
                foreach (Karta item in listaKarata)
                {
                    string line = item.JedinstveniIdentifikatorKarte;
                    string[] podaci = line.Split('.');
                    if (item.Status == StatusKarta.Odustanak && podaci[0] == korisnik.KorisnickoIme)
                    {
                        listaFiltriranih.Add(item);
                    }
                }
            }
            ViewBag.korisnici = listaFiltriranih;
            return View("~/Views/Korpa/Index.cshtml", listaFiltriranih);
        }

        public ActionResult PretragaKarata(string pretraga, string vrijednost)
        {
            List<Karta> listaKarata = Data.ProcitajKarte("~/App_Data/karte.txt");
            Korisnik korisnik = (Korisnik)Session["korisnik"];
            List<Karta> rezultat = new List<Karta>();

            if (pretraga == "Naziv_manifestacije")
            {
                foreach (Karta item in listaKarata)
                {
                    string line = item.JedinstveniIdentifikatorKarte;
                    string[] podaci = line.Split('.');
                    if (item.Manifestacija.Naziv.ToUpper().Contains(vrijednost.ToUpper()) && item.Status==StatusKarta.Rezervisana && korisnik.KorisnickoIme==podaci[0])
                    {
                        rezultat.Add(item);
                    }
                }
            }
            ViewBag.korisnici = rezultat;
            return View("~/Views/Korpa/Index.cshtml", rezultat);
        }

        public ActionResult PretragaPoCijeni(double minCijena, double maxCijena)
        {
            Korisnik korisnik = (Korisnik)Session["korisnik"];
            List<Karta> listaKarata = Data.ProcitajKarte("~/App_Data/karte.txt");
            List<Karta> pretragaCijena = new List<Karta>();


            if (minCijena <= maxCijena)
            {
                foreach (Karta item in listaKarata)
                {
                    string line = item.JedinstveniIdentifikatorKarte;
                    string[] podaci = line.Split('.');
                    if (item.Status==StatusKarta.Rezervisana && korisnik.KorisnickoIme==podaci[0])
                    {
                        if (item.Cijena >= minCijena && item.Cijena <= maxCijena)
                        {
                            pretragaCijena.Add(item);
                        }
                    }


                }
            }
            else
            {
                foreach (Karta item in listaKarata)
                {
                    string line = item.JedinstveniIdentifikatorKarte;
                    string[] podaci = line.Split('.');
                    if (item.Status==StatusKarta.Rezervisana && korisnik.KorisnickoIme == podaci[0])
                    {
                        if (item.Cijena >= maxCijena && item.Cijena <= minCijena)
                        {
                            pretragaCijena.Add(item);
                        }
                    }

                }
            }
            // }
            ViewBag.karte = pretragaCijena;
            return View("~/Views/Korpa/Index.cshtml", pretragaCijena);
        }
        public ActionResult PretragaPoDatumu(DateTime minDatum, DateTime maxDatum)
        {
            List<Karta> listaKarata = Data.ProcitajKarte("~/App_Data/karte.txt");
            List<Karta> pretragaDatum = new List<Karta>();
            Korisnik korisnik = (Korisnik)Session["korisnik"];

            if (minDatum <= maxDatum)
            {
                foreach (Karta item in listaKarata)
                {
                    string line = item.JedinstveniIdentifikatorKarte;
                    string[] podaci = line.Split('.');
                    if (item.Status == StatusKarta.Rezervisana && korisnik.KorisnickoIme == podaci[0])
                    {
                        if (item.Manifestacija.DatumVrijemeOdrzavanja.Date >= minDatum && item.Manifestacija.DatumVrijemeOdrzavanja.Date <= maxDatum)
                        {
                            pretragaDatum.Add(item);
                        }
                    }
                }
            }
            else
            {
                foreach (Karta item in listaKarata)
                {
                    string line = item.JedinstveniIdentifikatorKarte;
                    string[] podaci = line.Split('.');
                    if (item.Status == StatusKarta.Rezervisana && korisnik.KorisnickoIme == podaci[0])
                    {
                        if (item.Manifestacija.DatumVrijemeOdrzavanja.Date >= maxDatum && item.Manifestacija.DatumVrijemeOdrzavanja.Date <= minDatum)
                        {
                            pretragaDatum.Add(item);
                        }
                    }
                }
            }
            // }
            ViewBag.karte = pretragaDatum;
            return View("~/Views/Korpa/Index.cshtml", pretragaDatum);
        }
    }
}