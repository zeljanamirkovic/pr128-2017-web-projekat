﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using Projekat.Models;

namespace Projekat.Controllers
{
    public class AdministratorController : Controller
    {
        // GET: Administrator
        public ActionResult Index()
        {
            List<Korisnik> listaKorisnika = Data.ReadUsers("~/App_Data/korisnici.txt");
            List<Korisnik> rezultat = new List<Korisnik>();

            Korisnik korisnik = (Korisnik)Session["korisnik"];
            if (korisnik == null)
            {
                return RedirectToAction("Prijava", "Autentifikacija");
            }

            for(int i=0; i<listaKorisnika.Count; i++)
            {
                if (!listaKorisnika[i].Obrisan)
                {
                    if (listaKorisnika[i].Uloga != UlogaTip.Administrator)
                    {
                        rezultat.Add(listaKorisnika[i]);
                    }
                }
            }

            ViewBag.korisnici = rezultat;
            return View("~/Views/Administrator/Index.cshtml", rezultat); 
        }

        public ActionResult Filtriranje(string filter, string vrijednost)
        {
            List<Korisnik> koris = Data.ReadUsers("~/App_Data/korisnici.txt");
            List<Korisnik> listaKorisnika = new List<Korisnik>();

            foreach (Korisnik item in koris)
            {
                if (item.Obrisan == false && item.Uloga != UlogaTip.Administrator)
                {
                    listaKorisnika.Add(item);
                }
            }
            List<Korisnik> rezultat = new List<Korisnik>();

            if (filter == "Prodavac")
            {
                for (int i = 0; i < listaKorisnika.Count; i++)
                {
                    if (listaKorisnika[i].Uloga == UlogaTip.Prodavac)
                    {
                        rezultat.Add(listaKorisnika[i]);
                    }
                }
            }
            else if (filter == "Kupac")
            {
                for (int i = 0; i < listaKorisnika.Count; i++)
                {
                    if (listaKorisnika[i].Uloga == UlogaTip.Kupac)
                    {
                        rezultat.Add(listaKorisnika[i]);
                    }
                }
            }
            else if (filter == "Bronzani")
            {
                for (int i = 0; i < listaKorisnika.Count; i++)
                {
                    if (listaKorisnika[i].TipKorisnika.ImeTipa == TKImeTip.Bronzani)
                    {
                        rezultat.Add(listaKorisnika[i]);
                    }
                }
            }
            else if (filter == "Srebrni")
            {

                for (int i = 0; i < listaKorisnika.Count; i++)
                {
                    if (listaKorisnika[i].TipKorisnika.ImeTipa == TKImeTip.Srebrni)
                    {
                        rezultat.Add(listaKorisnika[i]);
                    }
                }
            }
            else if (filter == "Zlatni")
            {
                for (int i = 0; i < listaKorisnika.Count; i++)
                {
                    if (listaKorisnika[i].TipKorisnika.ImeTipa == TKImeTip.Zlatni)
                    {
                        rezultat.Add(listaKorisnika[i]);
                    }
                }
            }

            ViewBag.korisnici = rezultat;
            return View("~/Views/Administrator/Index.cshtml", rezultat);
        }

        public ActionResult Pretraga(string pretraga, string vrijednost)
        {
            List<Korisnik> listaKorisnika = Data.ReadUsers("~/App_Data/korisnici.txt");
            List<Korisnik> korisnici = new List<Korisnik>();

            for (int i = 0; i < listaKorisnika.Count; i++)
            {
                if (!listaKorisnika[i].Obrisan)
                {
                    if (listaKorisnika[i].Uloga != UlogaTip.Administrator)
                    {
                        korisnici.Add(listaKorisnika[i]);
                    }
                }
            }
         
            List<Korisnik> rezultat = new List<Korisnik>();

            if (pretraga == "Ime")
            {
                for(int i=0; i<korisnici.Count; i++)
                {
                    if (korisnici[i].Ime.ToUpper().Contains(vrijednost.ToUpper()))
                    {
                        rezultat.Add(korisnici[i]);
                    }
                }
            } else if (pretraga == "Prezime")
            {
                for (int i = 0; i < korisnici.Count; i++)
                {
                    if (korisnici[i].Prezime.ToUpper().Contains(vrijednost.ToUpper()))
                    {
                        rezultat.Add(korisnici[i]);
                    }
                }
            } else if(pretraga== "KorisnickoIme")
            {
                for (int i = 0; i < korisnici.Count; i++)
                {
                    if (korisnici[i].KorisnickoIme.ToUpper().Contains(vrijednost.ToUpper()))
                    {
                        rezultat.Add(korisnici[i]);
                    }
                }
            }

            ViewBag.korisnici = rezultat;
            return View("~/Views/Administrator/Index.cshtml", rezultat);
        }
//-------------------------------------------SORTIRANJE------------------------------------------------------------------------------------
        public ActionResult Sortiranje(string sortiranje, string nacin)
        {
            List<Korisnik> listaKorisnika = Data.ReadUsers("~/App_Data/korisnici.txt");
            List<Korisnik> korisnici = new List<Korisnik>();

            foreach (Korisnik item in listaKorisnika)
            {
                if (item.Obrisan == false)
                {
                    korisnici.Add(item);
                }
            }
            List<Korisnik> sortirani = new List<Korisnik>();

            List<Korisnik> rezultat = new List<Korisnik>();

            if (sortiranje == "Ime")
            {
                if (nacin == "Opadajuce")
                {
                    sortirani = korisnici.OrderByDescending(sort => sort.Ime).ToList();

                    for (int i = 0; i < sortirani.Count; i++)
                    {
                       if (sortirani[i].Uloga != UlogaTip.Administrator)
                       {
                         rezultat.Add(sortirani[i]);
                       }

                    }
                }
                else if (nacin == "Rastuce")
                {
                    sortirani = korisnici.OrderBy(sort => sort.Ime).ToList();

                    for (int i = 0; i < sortirani.Count; i++)
                    {
                        if (sortirani[i].Uloga != UlogaTip.Administrator)
                        {
                            rezultat.Add(sortirani[i]);
                        }

                    }
                }
                else
                {
                    sortirani = korisnici.OrderBy(sort => sort.Ime).ToList();

                    for (int i = 0; i < sortirani.Count; i++)
                    {
                        if (sortirani[i].Uloga != UlogaTip.Administrator)
                        {
                            rezultat.Add(sortirani[i]);
                        }

                    }
                }
            }
            else if (sortiranje == "Prezime")
            {
                if (nacin == "Opadajuce")
                {
                    sortirani = korisnici.OrderByDescending(sort => sort.Prezime).ToList();

                    for (int i = 0; i < sortirani.Count; i++)
                    {
                        if (sortirani[i].Uloga != UlogaTip.Administrator)
                        {
                            rezultat.Add(sortirani[i]);
                        }

                    }
                }
                else if (nacin == "Rastuce")
                {
                    sortirani = korisnici.OrderBy(sort => sort.Prezime).ToList();

                    for (int i = 0; i < sortirani.Count; i++)
                    {
                        if (sortirani[i].Uloga != UlogaTip.Administrator)
                        {
                            rezultat.Add(sortirani[i]);
                        }

                    }
                }
                else
                {
                    sortirani = korisnici.OrderBy(sort => sort.Prezime).ToList();

                    for (int i = 0; i < sortirani.Count; i++)
                    {
                        if (sortirani[i].Uloga != UlogaTip.Administrator)
                        {
                            rezultat.Add(sortirani[i]);
                        }

                    }
                }
            }
            else if (sortiranje == "KorisnickoIme")
            {
                if (nacin == "Opadajuce")
                {
                    sortirani = korisnici.OrderByDescending(sort => sort.KorisnickoIme).ToList();

                    for (int i = 0; i < sortirani.Count; i++)
                    {
                        if (sortirani[i].Uloga != UlogaTip.Administrator)
                        {
                            rezultat.Add(sortirani[i]);
                        }

                    }
                }
                else if (nacin == "Rastuce")
                {
                    sortirani = korisnici.OrderBy(sort => sort.KorisnickoIme).ToList();

                    for (int i = 0; i < sortirani.Count; i++)
                    {
                        if (sortirani[i].Uloga != UlogaTip.Administrator)
                        {
                            rezultat.Add(sortirani[i]);
                        }

                    }
                }
                else
                {
                    sortirani = korisnici.OrderBy(sort => sort.KorisnickoIme).ToList();

                    for (int i = 0; i < sortirani.Count; i++)
                    {
                        if (sortirani[i].Uloga != UlogaTip.Administrator)
                        {
                            rezultat.Add(sortirani[i]);
                        }

                    }
                }
            }
            else if (sortiranje == "BrojBodova")
            {
                if (nacin == "Opadajuce")
                {
                    sortirani = korisnici.OrderByDescending(sort => sort.BrojSakupljenihBodova).ToList();

                    for (int i = 0; i < sortirani.Count; i++)
                    {
                        if (sortirani[i].Uloga != UlogaTip.Administrator)
                        {
                            rezultat.Add(sortirani[i]);
                        }

                    }
                }
                else if (nacin == "Rastuce")
                {
                    sortirani = korisnici.OrderBy(sort => sort.BrojSakupljenihBodova).ToList();

                    for (int i = 0; i < sortirani.Count; i++)
                    {
                        if (sortirani[i].Uloga != UlogaTip.Administrator)
                        {
                            rezultat.Add(sortirani[i]);
                        }

                    }
                }
                else
                {
                    sortirani = korisnici.OrderBy(sort => sort.BrojSakupljenihBodova).ToList();

                    for (int i = 0; i < sortirani.Count; i++)
                    {
                        if (sortirani[i].Uloga != UlogaTip.Administrator)
                        {
                            rezultat.Add(sortirani[i]);
                        }

                    }
                }
            }

            ViewBag.korisnici = rezultat;
            return View("~/Views/Administrator/Index.cshtml", rezultat);
        }

        public ActionResult Modifikacija()
        {
            Korisnik korisnik = (Korisnik)Session["korisnik"];

            List<Korisnik> listaKorisnika = Data.ReadUsers("~/App_Data/korisnici.txt");
            Korisnik izmijenjen = new Korisnik();
            for (int i = 0; i < listaKorisnika.Count; i++)
            {
                if (listaKorisnika[i].Obrisan == false && listaKorisnika[i].KorisnickoIme.ToUpper() == korisnik.KorisnickoIme.ToUpper())
                {
                    izmijenjen = listaKorisnika[i];
                    break;
                }
            }

            return View("~/Views/Administrator/Modifikacija.cshtml", izmijenjen);
        }

        [HttpPost]
        public ActionResult Modifikacija(Korisnik korisnik)
        {
            Korisnik k = (Korisnik)Session["korisnik"];
            List<Korisnik> listaKorisnika = Data.ReadUsers("~/App_Data/korisnici.txt");

            if (korisnik.KorisnickoIme == null || korisnik.Lozinka == null || korisnik.Ime == null || korisnik.Prezime == null)
            {
                //ViewBag.Message = $"Ne sme biti prazno polje";
                return RedirectToAction("Modifikacija", "Administrator");
            }

            listaKorisnika.Find(kor => kor.KorisnickoIme.Equals(k.KorisnickoIme) && kor.Obrisan == false).Lozinka = korisnik.Lozinka;
            listaKorisnika.Find(kor => kor.KorisnickoIme.Equals(k.KorisnickoIme) && kor.Obrisan == false).Ime = korisnik.Ime;
            listaKorisnika.Find(kor => kor.KorisnickoIme.Equals(k.KorisnickoIme) && kor.Obrisan == false).Prezime = korisnik.Prezime;
            listaKorisnika.Find(kor => kor.KorisnickoIme.Equals(k.KorisnickoIme) && kor.Obrisan == false).Pol = korisnik.Pol;
            listaKorisnika.Find(kor => kor.KorisnickoIme.Equals(k.KorisnickoIme) && kor.Obrisan == false).DatumRodjenja = korisnik.DatumRodjenja;

            string path = HostingEnvironment.MapPath("~/App_Data/korisnici.txt");
            FileStream stream = new FileStream(path, FileMode.Create);
            StreamWriter sw = new StreamWriter(stream);
            foreach (Korisnik kor in listaKorisnika)
            {

                sw.WriteLine(kor.KorisnickoIme + ";" + kor.Lozinka + ";" + kor.Ime + ";" +
               kor.Prezime + ";" + kor.Pol + ";" + kor.DatumRodjenja + ";" + kor.Uloga + ";" + ";" + ";" + kor.BrojSakupljenihBodova + ";" + kor.TipKorisnika.ImeTipa + ";" + kor.Obrisan);
            }
            sw.Close();
            stream.Close();
            ViewBag.poruka = "Uspjesno ste promijenili podatke!";
            return View("~/Views/Administrator/Modifikacija.cshtml", korisnik);
        }

        public ActionResult ListaSvihManfestacija()
        {
            List<Manifestacija> listaManifestacija = Data.ProcitajManifestacije("~/App_Data/manifestacije.txt");
            List<Manifestacija> lista = new List<Manifestacija>();

            List<Manifestacija> datumV = new List<Manifestacija>();
            List<Manifestacija> sortiranoV = new List<Manifestacija>();

            List<Manifestacija> datumM = new List<Manifestacija>();
            List<Manifestacija> sortiranoM = new List<Manifestacija>();

            for (int i = 0; i < listaManifestacija.Count; i++)
            {
                if (listaManifestacija[i].Obrisana == false && listaManifestacija[i].DatumVrijemeOdrzavanja >= DateTime.Now)
                {
                    datumV.Add(listaManifestacija[i]);
                }
                else if (listaManifestacija[i].Obrisana == false && listaManifestacija[i].DatumVrijemeOdrzavanja < DateTime.Now)
                {
                    datumM.Add(listaManifestacija[i]);
                }
            }

            sortiranoV = datumV.OrderBy(sort => sort.DatumVrijemeOdrzavanja).ToList();
            lista = sortiranoV;

            sortiranoM = datumM.OrderByDescending(sort => sort.DatumVrijemeOdrzavanja).ToList();

            for (int i = 0; i < sortiranoM.Count; i++)
            {
                lista.Add(sortiranoM[i]);
            }

            ViewBag.manifestacije = lista;
            return View("~/Views/Administrator/ListaSvihManfestacija.cshtml", lista);
        }

        //---------------------------------------------------------------------------------------
        public ActionResult NoviProdavac()
        {
            //Korisnik korisnik = new Korisnik();
            //Session["korisnik"] = korisnik;
            //return View(korisnik);
            return View("~/Views/Administrator/NoviProdavac.cshtml");
        }

        [HttpPost]
        public ActionResult NoviProdavac(Korisnik korisnik)
        {
            List<Korisnik> listaKorisnika = Data.ReadUsers("~/App_Data/korisnici.txt");

            if (korisnik.KorisnickoIme == null || korisnik.Lozinka==null || korisnik.Ime==null
                || korisnik.Prezime==null ||  korisnik.DatumRodjenja==null)
            {
                ViewBag.Message = "Morate popuniti sva polja!";
                return View();
            }else
            {
                for(int i=0; i<listaKorisnika.Count; i++)
                {
                    if(listaKorisnika[i].Obrisan==false && listaKorisnika[i].KorisnickoIme.ToUpper() == korisnik.KorisnickoIme.ToUpper())
                    {
                        ViewBag.MessageKIme = $"Korisnik sa korisnickim imenom '{korisnik.KorisnickoIme}' postoji u bazi!";
                        return View();
                    }
                }

                if(korisnik.Pol.ToString()!="Muski" && korisnik.Pol.ToString() != "Zenski")
                {
                    ViewBag.Pol = "Izaberite jednu od ponudjenih vrijednosti!";
                    return View();
                }
                else if (!Regex.Match(korisnik.Ime, "^[A-Z][a-zA-Z]*$").Success)
                {
                    ViewBag.Message1 = "Ime mora zapoceti velikim pocetnim slovom! Nisu dozvoljeni brojevi!";
                    return View();
                }
                else if (!Regex.Match(korisnik.Prezime, "^[A-Z][a-zA-Z]*$").Success)
                {
                    ViewBag.Message2 = "Prezime mora zapoceti velikim pocetnim slovom! Nisu dozvoljeni brojevi!";
                    return View();
                }
               
            }
            korisnik.TipKorisnika.ImeTipa = TKImeTip.Bronzani;
            listaKorisnika.Add(korisnik);
            ViewBag.korisnici = listaKorisnika;
            Data.SacuvajProdavca(korisnik);
            return RedirectToAction("Index", "Administrator");
        }
        
       
        public ActionResult PromjeniStatus(string lokacija, string datumVrijemeOdrzavanja)
        {
            List<Manifestacija> listaManifestacija = Data.ProcitajManifestacije("~/App_Data/manifestacije.txt");
            Manifestacija man = new Manifestacija();
            for (int i = 0; i < listaManifestacija.Count; i++)
            {
                if (listaManifestacija[i].Obrisana == false && listaManifestacija[i].DatumVrijemeOdrzavanja.ToString() == datumVrijemeOdrzavanja && listaManifestacija[i].Lokacija.ToUpper() == lokacija.ToUpper())
                {
                    man = listaManifestacija[i];
                    break;
                }
            }
          
            if (man.DatumVrijemeOdrzavanja > DateTime.Now)
            {
                listaManifestacija.Find(m => m.Lokacija.ToString().Equals(lokacija) && m.DatumVrijemeOdrzavanja.ToString().Equals(datumVrijemeOdrzavanja) && m.Obrisana==false).Status = StatusTip.Aktivno;
                    string path = HostingEnvironment.MapPath("~/App_Data/manifestacije.txt");
                    FileStream stream = new FileStream(path, FileMode.Create);
                    StreamWriter sw = new StreamWriter(stream);

                    foreach (Manifestacija manifestacija in listaManifestacija)
                    {
                        sw.WriteLine(manifestacija.Naziv + ";" + manifestacija.TipManifestacije + ";" + manifestacija.BrojMjesta + ";"
                        + manifestacija.DatumVrijemeOdrzavanja + ";" + manifestacija.CijenaRegularKarte + ";" + manifestacija.Status + ";" + manifestacija.MjestoOdrzavanja + ";"
                        + manifestacija.Lokacija+";"+manifestacija.KorisnickoIme+";"+manifestacija.Ocjena+";"+manifestacija.Obrisana + ";" + manifestacija.BrojRezervisanihKarata);      //}

                    }
                    sw.Close();
                    stream.Close();
                
            }

            return RedirectToAction("ListaSvihManfestacija", "Administrator");                
        }

        [HttpPost]
        public ActionResult BrisanjeManifestacije(string lokacija, string datumVrijemeOdrzavanja)
        {
            List<Manifestacija> listaManifestacija = Data.ProcitajManifestacije("~/App_Data/manifestacije.txt");
            for (int i = 0; i < listaManifestacija.Count; i++)
            {
                if (listaManifestacija[i].Obrisana == false && listaManifestacija[i].Lokacija == lokacija && listaManifestacija[i].DatumVrijemeOdrzavanja.ToString() == datumVrijemeOdrzavanja)
                {
                    listaManifestacija[i].Obrisana = true;
                }
            }
            string putanja = HostingEnvironment.MapPath("~/App_Data/manifestacije.txt");
            FileStream stream = new FileStream(putanja, FileMode.Create);
            StreamWriter writer = new StreamWriter(stream);

            foreach (Manifestacija item in listaManifestacija)
            {
                writer.WriteLine(item.Naziv + ";" + item.TipManifestacije + ";" + item.BrojMjesta + ";"
                + item.DatumVrijemeOdrzavanja + ";" + item.CijenaRegularKarte + ";" + item.Status + ";" + item.MjestoOdrzavanja + ";"
                + item.Lokacija + ";" + item.KorisnickoIme + ";" + item.Ocjena + ";" + item.Obrisana + ";" + item.BrojRezervisanihKarata);

            }


            writer.Close();
            stream.Close();

            return RedirectToAction("ListaSvihManfestacija", "Administrator");
        }
        public ActionResult ListaSvihKarata(string lokacija, string datumVrijemeOdrzavanja)
        {
            List<Karta> listaKarata = Data.ProcitajKarte("~/App_Data/karte.txt");
            List<Karta> izlaz = new List<Karta>();
         
            for(int i=0; i<listaKarata.Count; i++)
            {
                if(listaKarata[i].Manifestacija.Lokacija==lokacija && listaKarata[i].Manifestacija.DatumVrijemeOdrzavanja.ToString() == datumVrijemeOdrzavanja)
                {
                    izlaz.Add(listaKarata[i]);
                }
            }
            ViewBag.karte = izlaz;
            return View("~/Views/Administrator/ListaSvihKarata.cshtml", izlaz);
            
        }

        public ActionResult ListaKomentara(string lokacija, string datumVrijemeOdrzavanja)
        {
            List<Komentar> listaKomentara = Data.ProcitajKomentare("~/App_Data/komentari.txt");
            List<Manifestacija> listaManifestacija = Data.ProcitajManifestacije("~/App_Data/manifestacije.txt");
            List<Komentar> lista = new List<Komentar>();
          
            for(int i=0; i<listaKomentara.Count; i++)
            {
                if(listaKomentara[i].ManifestacijaKomentar.Lokacija==lokacija && listaKomentara[i].ManifestacijaKomentar.DatumVrijemeOdrzavanja.ToString() == datumVrijemeOdrzavanja)
                {
                    lista.Add(listaKomentara[i]);
                }
            }

            ViewBag.komentari = lista;
            return View("~/Views/Administrator/ListaSvihKomentara.cshtml", lista);
        }

        [HttpPost]
        public ActionResult BrisanjeKorisnika(string korisnickoIme)
        {
           List<Korisnik> listaKorisnika = Data.ReadUsers("~/App_Data/korisnici.txt");
           for(int i=0; i<listaKorisnika.Count; i++)
            {
                if(listaKorisnika[i].KorisnickoIme == korisnickoIme)
                {
                    listaKorisnika[i].Obrisan = true;
                }
            }
            string putanja = HostingEnvironment.MapPath("~/App_Data/korisnici.txt");
            FileStream stream = new FileStream(putanja, FileMode.Create);
            StreamWriter writer = new StreamWriter(stream);
            foreach (Korisnik item in listaKorisnika)
            {
                writer.WriteLine(item.KorisnickoIme + ";" + item.Lozinka + ";" + item.Ime + ";" +
               item.Prezime + ";" + item.Pol + ";" + item.DatumRodjenja + ";" + item.Uloga + ";" + ";" + ";" + item.BrojSakupljenihBodova + ";"
               + item.TipKorisnika.ImeTipa + ";" + item.Obrisan);
            }
            writer.Close();
            stream.Close();
            return RedirectToAction("Index", "Administrator");
        }
    }
}